package com.spidertechnosoft.xenia.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;
import com.spidertechnosoft.xenia.view.adapter.OrderCartListAdapter;
import com.spidertechnosoft.xenia.view.adapter.OrderDetailListAdapter;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class OrderDetailFragment extends Fragment implements CartOperationCallback {

    private Context mContext;
    private Order mOrder;
    List<CartItem> cartItems=new ArrayList<>();
    private OnFragmentInteractionListener mListener;

    private View mRootView;

    private LayoutInflater mCurrentInflater;

    Database mDatabase;

    TextView txtOrderDetailKOTNo;

    TextView txtOrderDetailTable;

    TextView txtOrderDetailDate;

    EditText editOrderDetailPax;

    ListView lvOrderDetailsList;

    RelativeLayout btnOrderDetailItemAdd;

    RelativeLayout btnOrderDetailItemDelete;

    RelativeLayout orderDetailTableContainer;

    RelativeLayout orderDetailPaxContainer;

    OrderDetailListAdapter mOrderDetailListAdapter;

    CartItem mCartItemSelected;

    ImageView btnTableChange;

    public OrderDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static OrderDetailFragment newInstance() {
        OrderDetailFragment fragment = new OrderDetailFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Save the context
        this.mContext = getActivity();

        this.mDatabase=Database.getInstance();

        this.mOrder=((PlaceOrderActivity)mContext).getOrder();

        List<CartItem> cartItemList=mDatabase.getCartContent();

        if(cartItemList!=null){

            cartItems.addAll(cartItemList);

        }

        // Store the inflater reference
        this.mCurrentInflater = inflater;

        this.mRootView = inflater.inflate(R.layout.fragment_order_detail, container, false);

        configView();

        return mRootView;
    }

    public void configView(){

        orderDetailTableContainer=(RelativeLayout)mRootView.findViewById(R.id.orderDetailTableContainer);

        orderDetailPaxContainer=(RelativeLayout)mRootView.findViewById(R.id.orderDetailPaxContainer);

        btnTableChange=(ImageView)mRootView.findViewById(R.id.btnTableChange);

        btnTableChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.goToFragment(null, PlaceOrderActivity.PlaceOrderFragments.TABLE_FRAGMENT);
            }
        });


        txtOrderDetailKOTNo=(TextView)mRootView.findViewById(R.id.txtOrderDetailKOTNo);

        txtOrderDetailTable=(TextView)mRootView.findViewById(R.id.txtOrderDetailTable);

        txtOrderDetailDate=(TextView)mRootView.findViewById(R.id.txtOrderDetailDate);

        editOrderDetailPax=(EditText)mRootView.findViewById(R.id.editOrderDetailPax);

        editOrderDetailPax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                ((PlaceOrderActivity)mContext).getOrder().setPax(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnOrderDetailItemAdd=(RelativeLayout)mRootView.findViewById(R.id.btnOrderDetailItemAdd);

        btnOrderDetailItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.goToFragment(null, PlaceOrderActivity.PlaceOrderFragments.PRODUCT_FRAGMENT);
            }
        });

        btnOrderDetailItemDelete=(RelativeLayout)mRootView.findViewById(R.id.btnOrderDetailItemDelete);

        btnOrderDetailItemDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCartItemSelected==null){

                    return;
                }

                if(reduceAndRemoveFromCart(mCartItemSelected.getProduct())){

                    mCartItemSelected=null;
                    btnOrderDetailItemDelete.setEnabled(false);
                }

            }
        });

        txtOrderDetailKOTNo.setText((mOrder.getOrderNo()==null||mOrder.getOrderNo().trim().isEmpty())?"XXXXXXXX":mOrder.getOrderNo());


        if(mOrder.getDate()==null||mOrder.getDate().trim().isEmpty()){

            txtOrderDetailDate.setText(GeneralMethods.getDateInSpecifiedFormat(new Date(),GeneralMethods.LOCAL_DATE_TIME_FORMAT));
        }else{
            txtOrderDetailDate.setText(GeneralMethods.convertDateFormat(mOrder.getDate(),GeneralMethods.SERVER_DATE_TIME_FORMAT,GeneralMethods.LOCAL_DATE_TIME_FORMAT));
        }

        lvOrderDetailsList=(ListView)mRootView.findViewById(R.id.lvOrderDetailsList);

        lvOrderDetailsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mCartItemSelected=mOrderDetailListAdapter.getItem(position);

                if(lvOrderDetailsList.getCheckedItemCount()==0)
                    btnOrderDetailItemDelete.setEnabled(false);
                else
                    btnOrderDetailItemDelete.setEnabled(true);
            }
        });

        mOrderDetailListAdapter=new OrderDetailListAdapter(mContext,cartItems,mOrder.getType(),this);

        lvOrderDetailsList.setAdapter(mOrderDetailListAdapter);

        if(lvOrderDetailsList.getCheckedItemCount()==0)
            btnOrderDetailItemDelete.setEnabled(false);
        else
            btnOrderDetailItemDelete.setEnabled(true);

        if (mOrder.getType().equals(OrderType.DINE_IN_NORMAL) || mOrder.getType().equals(OrderType.DINE_IN_AC)) {
            orderDetailTableContainer.setVisibility(View.VISIBLE);
            orderDetailPaxContainer.setVisibility(View.VISIBLE);
            txtOrderDetailTable.setText(mOrder.getTable());
            editOrderDetailPax.setText((mOrder.getPax()==null||mOrder.getPax().trim().isEmpty())?"":mOrder.getPax());
        }else{
            orderDetailTableContainer.setVisibility(View.GONE);
            orderDetailPaxContainer.setVisibility(View.GONE);
            txtOrderDetailTable.setText("");
            editOrderDetailPax.setText("");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void setFooterCartTotal();
        Double getProductPrice(Product product);
        void goToFragment(Bundle bundle,PlaceOrderActivity.PlaceOrderFragments uri);
    }

    @Override
    public boolean reduceAndRemoveFromCart(Product product) {
        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        List<CartItem> cartItemList=mDatabase.getCartContent();

        cartItems.clear();

        if(cartItemList!=null){

            cartItems.addAll(cartItemList);
        }

        mOrderDetailListAdapter=new OrderDetailListAdapter(mContext,cartItems,mOrder.getType(),this);

        lvOrderDetailsList.setAdapter(mOrderDetailListAdapter);

        mOrderDetailListAdapter.notifyDataSetChanged();

        if(lvOrderDetailsList.getCheckedItemCount()==0)
            btnOrderDetailItemDelete.setEnabled(false);
        else
            btnOrderDetailItemDelete.setEnabled(true);

        mListener.setFooterCartTotal();

        return true;
    }

    @Override
    public boolean removeFromCart(Product product) {
        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        List<CartItem> cartItemList=mDatabase.getCartContent();

        cartItems.clear();

        if(cartItemList!=null){

            cartItems.addAll(cartItemList);
        }

        mOrderDetailListAdapter=new OrderDetailListAdapter(mContext,cartItems,mOrder.getType(),this);

        lvOrderDetailsList.setAdapter(mOrderDetailListAdapter);

        mOrderDetailListAdapter.notifyDataSetChanged();

        mListener.setFooterCartTotal();

        return true;
    }

    @Override
    public Integer getCartQty(Product product) {
        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null||cartItem.getQty()==null) {

            return 0;
        }

        return (cartItem.getQty()==null?0:cartItem.getQty().intValue());
    }

    @Override
    public boolean increaseCartItem(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            cartItem=new CartItem();
            cartItem.setProduct(product);
            cartItem.setItemId(product.getItemId());
            cartItem.setItemCode(product.getItemCode());
            cartItem.setItemName(product.getItemName());
            cartItem.setCategoryId(product.getCategoryId());

            Double cartQty = 0.00;
            Double rate = mListener.getProductPrice(product);
            cartItem.setQty(cartQty);
            cartItem.setRate(rate);
            Double amount = cartQty*rate;
            cartItem.setAmount(amount);

        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty + 1;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return false;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

}
