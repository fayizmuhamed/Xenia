package com.spidertechnosoft.xenia.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.User;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;

import org.w3c.dom.Text;

import java.util.Date;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String FINISH_ORDER_CREATION = "com.spidertechnosoft.xenia.FINISH_ORDER_CREATION";

    private Database mDatabase;

    private Button btnDineInNormal;

    private Button btnDineInAc;

    private Button btnTakeAway;

    private Button btnDoorDelivery;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mDatabase=Database.getInstance();

        mDatabase.clearCart();

//        getSupportActionBar().setCustomView(R.id.action_bar_title);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        user=mDatabase.getLoggedInUser();
        TextView txtProfileName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.txtLoggedInUser);
        if(user==null||user.getUserName()==null||user.getUserName().isEmpty())
            txtProfileName.setText("");
        else
            txtProfileName.setText(user.getUserName());

        TextView txtLoggedInSystem=(TextView)navigationView.getHeaderView(0).findViewById(R.id.txtLoggedInSystem);

        String loggedInSystem=mDatabase.getBaseIp();

        if(loggedInSystem==null||loggedInSystem.isEmpty())
            txtLoggedInSystem.setText("");
        else
            txtLoggedInSystem.setText(loggedInSystem);

        configView();

    }

    public void configView(){

        btnDineInNormal=(Button)findViewById(R.id.btnDineInNormal);

        btnDineInAc=(Button)findViewById(R.id.btnDineInAc);

        btnTakeAway=(Button)findViewById(R.id.btnTakeAway);

        btnDoorDelivery=(Button)findViewById(R.id.btnDoorDelivery);

        btnDineInNormal.setOnClickListener(onOrderTypeClick);

        btnDineInAc.setOnClickListener(onOrderTypeClick);

        btnTakeAway.setOnClickListener(onOrderTypeClick);

        btnDoorDelivery.setOnClickListener(onOrderTypeClick);

    }
    View.OnClickListener onOrderTypeClick = new View.OnClickListener() {
        public void onClick(View v) {

            Order order=new Order();
            order.setOrderType("KOT");
            order.setOrderDate(GeneralMethods.getDateInSpecifiedFormat(new Date(),GeneralMethods.SERVER_DATE_TIME_FORMAT));
            order.setDate(GeneralMethods.getDateInSpecifiedFormat(new Date(),GeneralMethods.SERVER_DATE_TIME_FORMAT));
            order.setReady(false);
            order.setProcessed(false);
            order.setCancelled(false);
            if(user!=null){
                order.setWaiterId(user.getUserId());
                order.setWaiterName(user.getUserName());
            }

            switch (v.getId()){

                case R.id.btnDineInNormal:
                    order.setType(OrderType.DINE_IN_NORMAL);
                    break;
                case R.id.btnDineInAc:
                    order.setType(OrderType.DINE_IN_AC);
                    break;
                case R.id.btnTakeAway:
                    order.setType(OrderType.TAKE_AWAY);
                    break;
                case R.id.btnDoorDelivery:
                    order.setType(OrderType.DOOR_DELIVERY);
                    break;
            }
            Intent intent = new Intent(getApplicationContext(),PlaceOrderActivity.class);
            intent.putExtra(PlaceOrderActivity.ORDER,order);
            startActivity(intent);
        }
    };


    @Override
    public void onResume(){
        mDatabase.clearCart();
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pending_orders) {
            // Handle the camera action
            Intent intent = new Intent(getApplicationContext(),PendingOrdersActivity.class);
            startActivity(intent);
        }else if(id==R.id.nav_refresh_data) {
            Intent intent = new Intent(this,SyncActivity.class);
            startActivity(intent);

        }else if(id==R.id.nav_logout) {
            if(mDatabase.removeLoggedInUser()) {
                Intent intent = new Intent(HomeActivity.this, LauncherActivity.class);
                startActivity(intent);
                finish();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
