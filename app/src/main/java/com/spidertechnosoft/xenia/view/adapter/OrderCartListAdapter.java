package com.spidertechnosoft.xenia.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;
import com.spidertechnosoft.xenia.view.PlaceOrderActivity;
import com.spidertechnosoft.xenia.view.ProductDetailsActivity;
import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class OrderCartListAdapter extends ArrayAdapter<CartItem> {

    private final Context context;
    private final List<CartItem> cartItems;
    private Order mOrder;
    private CartOperationCallback mCartOperationCallback;

    public OrderCartListAdapter(@NonNull Context context, @NonNull List<CartItem> objects,@NonNull Order order,CartOperationCallback mCartOperationCallback) {
        super(context,-1, objects);
        this.context=context;
        this.cartItems=objects;
        this.mOrder=order;
        this.mCartOperationCallback=mCartOperationCallback;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.lay_order_cart_list_item, parent, false);

        ImageView imgProductImage=(ImageView)rowView.findViewById(R.id.imgProductImage);

        TextView lblProductName=(TextView) rowView.findViewById(R.id.lblProductName);

        TextView lblProductPrice=(TextView) rowView.findViewById(R.id.lblProductPrice);

        final TextView lblProductOrderedQty=(TextView) rowView.findViewById(R.id.lblProductOrderedQty);

        final ImageView btnProductAddToCart=(ImageView) rowView.findViewById(R.id.btnProductAddToCart);

        final ImageView btnProductReduceFromCart=(ImageView) rowView.findViewById(R.id.btnProductReduceFromCart);

        CartItem cartItem=cartItems.get(position);

        Product product=cartItem.getProduct();

        imgProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context.getApplicationContext(),ProductDetailsActivity.class);
                intent.putExtra(PlaceOrderActivity.ORDER,mOrder);

                intent.putExtra(PlaceOrderActivity.PRODUCT,product);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //finish();
                context.startActivity(intent);
            }
        });

        btnProductAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mCartOperationCallback.increaseCartItem(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                lblProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }


            }
        });

        btnProductReduceFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCartOperationCallback.reduceFromCart(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                lblProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }
            }
        });


        if(product!=null){

            Picasso.with(context)
                    .load(GeneralMethods.getImageUrl(product.getProductImage()))
                    .placeholder(R.color.empty_image_bg)
                    .error(R.color.empty_image_bg)
                    .fit()
                    .into(imgProductImage);

            lblProductName.setText(product.getItemName());

            String price="0.00";

            switch (mOrder.getType()){

                case OrderType.DINE_IN_NORMAL:
                    price=product.getsRate()==null?"0.00":product.getsRate().toString();
                    break;
                case OrderType.DINE_IN_AC:
                    price=product.getaCRate()==null?"0.00":product.getaCRate().toString();
                    break;
                case OrderType.TAKE_AWAY:
                case OrderType.DOOR_DELIVERY:
                    price=product.getParcelRate()==null?"0.00":product.getParcelRate().toString();
                    break;
            }
            lblProductPrice.setText(price+" INR");

            Integer cartQty=mCartOperationCallback.getCartQty(product);

            lblProductOrderedQty.setText(cartQty.toString());

            if(cartQty>0){

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(true);
            }else{

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(false);
            }



        }

        return rowView;
    }


}
