package com.spidertechnosoft.xenia.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.view.OrderFragment;
import com.spidertechnosoft.xenia.view.ProductFragment;
import com.spidertechnosoft.xenia.view.TableFragment;

/**
 * Created by DELL on 11/22/2017.
 */

public class OrderFragmentPagerAdapter extends FragmentStatePagerAdapter {


    private static int NUM_ITEMS = 3;


    public OrderFragmentPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return ProductFragment.newInstance("", "Page # 1");
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return OrderFragment.newInstance();
            case 2: // Fragment # 1 - This will show SecondFragment
                return TableFragment.newInstance();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {

    }
}
