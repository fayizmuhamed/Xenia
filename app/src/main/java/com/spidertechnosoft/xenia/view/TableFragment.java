package com.spidertechnosoft.xenia.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Table;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.view.adapter.TableSelectionAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TableFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TableFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableFragment extends Fragment {

    private Context mContext;

    private View mRootView;

    private Order mOrder;

    private LayoutInflater mCurrentInflater;


    private GridView gvTableSelection;

    private TableSelectionAdapter mTableSelectionAdapter;

    private List<Table> tables=new ArrayList<>();

    private Database mDatabase;

    private OnFragmentInteractionListener mListener;

    public TableFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TableFragment newInstance() {
        TableFragment fragment = new TableFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Save the context
        this.mContext = getActivity();

        // Store the inflater reference
        this.mCurrentInflater = inflater;

        this.mRootView = inflater.inflate(R.layout.fragment_table, container, false);


        this.mDatabase= Database.getInstance();

        this.mOrder=((PlaceOrderActivity)mContext).getOrder();
        tables.clear();
        switch (mOrder.getType()){

            case OrderType.DINE_IN_NORMAL:
                List<Table> normalTables=mDatabase.getTables("Normal",Table.Sort.BY_ID);

                if(normalTables!=null){

                    tables.addAll(normalTables);
                }
                break;
            case OrderType.DINE_IN_AC:
                List<Table> acTables=mDatabase.getTables("AC",Table.Sort.BY_ID);

                if(acTables!=null){

                    tables.addAll(acTables);
                }
                break;
        }


        configView();

        return mRootView;
    }

    private void configView(){

        gvTableSelection=(GridView)mRootView.findViewById(R.id.gvTableSelection);

        mTableSelectionAdapter=new TableSelectionAdapter(mContext,tables);

        gvTableSelection.setAdapter(mTableSelectionAdapter);

        gvTableSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Table table = (Table)mTableSelectionAdapter.getItem(position);
                ((PlaceOrderActivity)mContext).getOrder().setTableId(table.getTableId());
                ((PlaceOrderActivity)mContext).getOrder().setTable(table.getTable());
                mListener.goToFragment(null, PlaceOrderActivity.PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        public void goToFragment(Bundle bundle,PlaceOrderActivity.PlaceOrderFragments uri);
    }
}
