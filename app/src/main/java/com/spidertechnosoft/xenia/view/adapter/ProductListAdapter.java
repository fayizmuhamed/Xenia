package com.spidertechnosoft.xenia.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;
import com.spidertechnosoft.xenia.view.PlaceOrderActivity;
import com.spidertechnosoft.xenia.view.ProductDetailsActivity;
import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class ProductListAdapter extends BaseAdapter implements Filterable {

    private final Context context;
    private List<Product>originalData = null;
    private List<Product>filteredData = null;
    private LayoutInflater mInflater;
    private Order mOrder;

    private ItemFilter mFilter = new ItemFilter();

    private CartOperationCallback mCartOperationCallback;

    public ProductListAdapter(@NonNull Context context, @NonNull List<Product> data,@NonNull Order order,CartOperationCallback mCartOperationCallback) {
        this.context=context;

        this.mCartOperationCallback=mCartOperationCallback;

        this.originalData=data;
        this.filteredData=data;
        this.mOrder=order;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return filteredData.size();
    }

    public Object getItem(int position) {
        return filteredData.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View rowView = mInflater.inflate(R.layout.lay_product_list_item, parent, false);

        ImageView imgProductImage=(ImageView)rowView.findViewById(R.id.imgProductImage);

        TextView lblProductName=(TextView) rowView.findViewById(R.id.lblProductName);

        TextView lblProductPrice=(TextView) rowView.findViewById(R.id.lblProductPrice);

        final TextView lblProductOrderedQty=(TextView) rowView.findViewById(R.id.lblProductOrderedQty);

        final ImageView btnProductAddToCart=(ImageView) rowView.findViewById(R.id.btnProductAddToCart);

        final ImageView btnProductReduceFromCart=(ImageView) rowView.findViewById(R.id.btnProductReduceFromCart);

        imgProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Product product=filteredData.get(position);

                Intent intent = new Intent(context.getApplicationContext(),ProductDetailsActivity.class);

                intent.putExtra(PlaceOrderActivity.ORDER,mOrder);

                intent.putExtra(PlaceOrderActivity.PRODUCT,product);
                context.startActivity(intent);
            }
        });

        btnProductAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Product product=filteredData.get(position);

                mCartOperationCallback.increaseCartItem(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                lblProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }
            }
        });

        btnProductReduceFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Product product=filteredData.get(position);

                mCartOperationCallback.reduceAndRemoveFromCart(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                lblProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }
            }
        });

        Product product=filteredData.get(position);

        if(product!=null){
            Picasso.with(context)
                    .load(GeneralMethods.getImageUrl(product.getProductImage()))
                    .placeholder(R.color.empty_image_bg)
                    .error(R.color.empty_image_bg)
                    .fit()
                    .into(imgProductImage);
//            if(product.getProductImage()==null){
//
//                imgProductImage.setBackgroundResource(R.color.empty_image_bg);
//            }else{
//
//
//            }

            lblProductName.setText(product.getItemName());

            Double price=getProductPrice(product);

            lblProductPrice.setText(price+" INR");

            Integer cartQty=mCartOperationCallback.getCartQty(product);

            lblProductOrderedQty.setText(cartQty.toString());

            if(cartQty>0){

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(true);
            }else{

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(false);
            }




        }

        return rowView;
    }

    public Filter getFilter() {
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Product> list = originalData;

            int count = list.size();
            final ArrayList<Product> nList = new ArrayList<Product>(count);

            Product filterableProduct ;

            for (int i = 0; i < count; i++) {
                filterableProduct = list.get(i);
                if (filterableProduct.getItemName()!=null&&filterableProduct.getItemName().toLowerCase().contains(filterString)) {
                    nList.add(filterableProduct);
                }
            }

            results.values = nList;
            results.count = nList.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<Product>) results.values;
            notifyDataSetChanged();
        }

    }

    public Double getProductPrice(Product product){

        Double price=0.00;

        switch (mOrder.getType()){

            case OrderType.DINE_IN_NORMAL:
                price=product.getsRate()==null?0.00:product.getsRate();
                break;
            case OrderType.DINE_IN_AC:
                price=product.getaCRate()==null?0.00:product.getaCRate();
                break;
            case OrderType.TAKE_AWAY:
            case OrderType.DOOR_DELIVERY:
                price=product.getParcelRate()==null?0.00:product.getParcelRate();
                break;
            default:
                price=0.00;
        }

        return price;
    }
}
