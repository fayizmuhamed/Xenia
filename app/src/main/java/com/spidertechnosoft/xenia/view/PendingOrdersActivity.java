package com.spidertechnosoft.xenia.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.RestApiManager;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.view.adapter.OrderCartListAdapter;
import com.spidertechnosoft.xenia.view.adapter.PendingOrderListAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingOrdersActivity extends AppCompatActivity {

    List<Order> orders=new ArrayList<>(0);

    PendingOrderListAdapter mPendingOrderListAdapter;

    SwipeMenuListView lvOrdersList;

    SwipeMenuCreator mSwipeMenuCreator;

    RelativeLayout btnAddNewOrder;

    RelativeLayout btnPrintOrder;

    SwipeRefreshLayout pendingOrderRefreshContainer;

    //Get rest api manager
    RestApiManager mRestApiManager;

    Dialog dlgPrintingProgress;

    Order mOrderSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRestApiManager=new RestApiManager();

        initializeOrders();

        configView();

    }

    public void configView(){

        pendingOrderRefreshContainer=(SwipeRefreshLayout)findViewById(R.id.pendingOrderRefreshContainer);

        pendingOrderRefreshContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initializeOrders();
            }
        });

        mPendingOrderListAdapter=new PendingOrderListAdapter(this,orders);

        lvOrdersList=(SwipeMenuListView) findViewById(R.id.lvOrdersList);

        mSwipeMenuCreator=new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {

                SwipeMenuItem item1 = new SwipeMenuItem(getApplicationContext());
                item1.setWidth(getResources().getInteger(R.integer.order_delete_button_width));
                item1.setTitle(null);
                item1.setBackground(R.color.delete_button_bg);
                item1.setIcon(R.drawable.icon_delete);
                menu.addMenuItem(item1);

                SwipeMenuItem item2 = new SwipeMenuItem(getApplicationContext());
                item2.setWidth(getResources().getInteger(R.integer.order_delete_button_width));
                item2.setTitle(null);
                item2.setIcon(R.drawable.icon_edit);
                item2.setBackground(R.color.edit_button_bg);
                menu.addMenuItem(item2);

                SwipeMenuItem item3 = new SwipeMenuItem(getApplicationContext());
                item3.setWidth(getResources().getInteger(R.integer.order_delete_button_width));
                item3.setTitle(null);
                item3.setIcon(R.drawable.icon_narration);
                item3.setBackground(R.color.narration_button_bg);
                menu.addMenuItem(item3);
            }
        };

        lvOrdersList.setAdapter(mPendingOrderListAdapter);

        //set MenuCreator
        lvOrdersList.setMenuCreator(mSwipeMenuCreator);

        lvOrdersList.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                Order order = mPendingOrderListAdapter.getItem(position);
                switch (index) {
                    case 0:
                        cancelOrder(order);
                        break;

                    case 1:
                        editOrder(order);
                        break;
                    case 2:
                        Intent intent = new Intent(getApplicationContext(),FeedbackActivity.class);
                        intent.putExtra(PlaceOrderActivity.ORDER,order);
                        startActivity(intent);

                        break;
                }
                return false;
            }
        });

        btnAddNewOrder=(RelativeLayout)findViewById(R.id.btnAddNewOrder);

        btnAddNewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }
        });

        btnPrintOrder=(RelativeLayout)findViewById(R.id.btnPrintOrder);

        btnPrintOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mOrderSelected==null){

                    return;
                }


                printOrder(mOrderSelected);
            }
        });

        lvOrdersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mOrderSelected=mPendingOrderListAdapter.getItem(position);

                if(lvOrdersList.getCheckedItemCount()==0)
                    btnPrintOrder.setEnabled(false);
                else
                    btnPrintOrder.setEnabled(true);
            }
        });



    }

    public void initializeOrders(){

        ProgressDialog progressDialog=new ProgressDialog(PendingOrdersActivity.this);

        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching pending orders");
        progressDialog.show();

        //get user call
        Call<List<Order>> orderCall=mRestApiManager.getOrderClient().getPendingOrders();

        orderCall.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                if(response.isSuccessful()){

                    orders.clear();
                    List<Order> orderList=response.body();

                    if(orderList==null||orderList.isEmpty()){
                        progressDialog.dismiss();
                        pendingOrderRefreshContainer.setRefreshing(false);
                        mPendingOrderListAdapter=new PendingOrderListAdapter(getApplicationContext(),orders);
                        lvOrdersList.setAdapter(mPendingOrderListAdapter);
                        mPendingOrderListAdapter.notifyDataSetChanged();
                        return;
                    }

                    orders.addAll(orderList);
                    mPendingOrderListAdapter=new PendingOrderListAdapter(getApplicationContext(),orders);
                    lvOrdersList.setAdapter(mPendingOrderListAdapter);
                    mPendingOrderListAdapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();
                pendingOrderRefreshContainer.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Pending Order fetch failed",Toast.LENGTH_SHORT).show();
                t.printStackTrace();
                pendingOrderRefreshContainer.setRefreshing(false);
            }
        });


    }

    public void cancelOrder(Order order){

        ProgressDialog progressDialog=new ProgressDialog(PendingOrdersActivity.this);

        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Order Canceling...");
        progressDialog.show();

        Integer orderId=order.getOrderId();

        //get user call
        Call<String> cancelOrderCall=mRestApiManager.getOrderClient().cancelOrder(orderId);

        cancelOrderCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if(response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"Order cancel success",Toast.LENGTH_SHORT).show();
                    initializeOrders();
                }else{
                    Toast.makeText(getApplicationContext(),"Order cancel failed",Toast.LENGTH_SHORT).show();
                }

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Order cancel failed",Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


    public void printOrder(Order order){

        // Declare the customer dialog
        dlgPrintingProgress = new Dialog(PendingOrdersActivity.this);

        // 	SEt no title for the dialog
        dlgPrintingProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Set the content view to the customer_alert layout
        dlgPrintingProgress.setContentView(R.layout.layout_custom_printing_progress);

        ProgressBar progress=(ProgressBar)dlgPrintingProgress.findViewById(R.id.pbPrintingProgress);
        progress.setIndeterminate(true);

        // Cancel the dialog when touched outside.
        dlgPrintingProgress.setCanceledOnTouchOutside(false);

        dlgPrintingProgress.setCancelable(false);

        dlgPrintingProgress.show();

        Integer orderId=order.getOrderId();

        //get user call
        Call<String> printOrderCall=mRestApiManager.getOrderClient().printOrder(orderId);

        printOrderCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if(response.isSuccessful()){
                    Toast.makeText(getApplicationContext(),"Order printing success",Toast.LENGTH_SHORT).show();
                    //initializeOrders();
                }else{
                    Toast.makeText(getApplicationContext(),"Order printing failed",Toast.LENGTH_SHORT).show();
                }

                dlgPrintingProgress.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dlgPrintingProgress.dismiss();
                Toast.makeText(getApplicationContext(),"Order printing failed",Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });
    }

    public void editOrder(Order order){

        Intent intent = new Intent(getApplicationContext(),PlaceOrderActivity.class);
        intent.putExtra(PlaceOrderActivity.ORDER,order);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);

    }
}
