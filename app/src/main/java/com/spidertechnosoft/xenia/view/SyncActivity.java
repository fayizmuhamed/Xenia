package com.spidertechnosoft.xenia.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.RestApiManager;
import com.spidertechnosoft.xenia.model.api.resource.Category;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.api.resource.Table;
import com.spidertechnosoft.xenia.model.api.resource.User;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.model.service.DataFetchService;
import com.spidertechnosoft.xenia.model.service.SyncStatus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncActivity extends AppCompatActivity {

    public static final String SYNC_PROGRESS = "sync_progress";

    ProgressBar pbDataSyncProgress;

    TextView txtProgressText;

    TextView txtError;

    LinearLayout laySyncStatus;
    LinearLayout laySyncRetryContainer;

    LinearLayout laySyncConfiguration;

    RelativeLayout laySyncProgress;

    TextView txtSyncBaseUrl;

    Button btnSync;

    Button btnRetry;

    private Database mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mDatabase=Database.getInstance();

        configView();

        registerReceiver();

        if(mDatabase.isFirstTimeUse()){

            laySyncConfiguration.setVisibility(View.VISIBLE);
            laySyncProgress.setVisibility(View.GONE);

        }else{
            laySyncConfiguration.setVisibility(View.GONE);
            laySyncProgress.setVisibility(View.VISIBLE);
            startDataFetch();
        }



    }

    public void configView(){

        laySyncConfiguration=(LinearLayout)findViewById(R.id.laySyncConfiguration);
        laySyncConfiguration.setVisibility(View.GONE);
        txtSyncBaseUrl=(EditText)findViewById(R.id.txtSyncBaseUrl);
        btnSync=(Button)findViewById(R.id.btnSync);
        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String base=txtSyncBaseUrl.getText().toString();

                if(base==null||base.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter base url",Toast.LENGTH_SHORT).show();
                    return;
                }
                mDatabase.setBaseUrl(base);
                laySyncConfiguration.setVisibility(View.GONE);
                laySyncProgress.setVisibility(View.VISIBLE);
                startDataFetch();
            }
        });
        laySyncProgress=(RelativeLayout)findViewById(R.id.laySyncProgress);
        laySyncProgress.setVisibility(View.GONE);

        laySyncStatus=(LinearLayout)findViewById(R.id.laySyncStatus);
        laySyncStatus.setVisibility(View.VISIBLE);
        pbDataSyncProgress=(ProgressBar)findViewById(R.id.pbDataSyncProgress);
        txtProgressText=(TextView)findViewById(R.id.txtProgressText);



        laySyncRetryContainer=(LinearLayout)findViewById(R.id.laySyncRetryContainer);
        laySyncRetryContainer.setVisibility(View.GONE);
        txtError=(TextView)findViewById(R.id.txtError);
        btnRetry=(Button)findViewById(R.id.btnRetry);
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startDataFetch();
            }
        });
    }


    private void startDataFetch(){
        laySyncStatus.setVisibility(View.VISIBLE);
        laySyncRetryContainer.setVisibility(View.GONE);
        txtError.setText("");
        txtProgressText.setText("");
        Intent intent = new Intent(this,DataFetchService.class);
        startService(intent);

    }

    private void registerReceiver(){

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SYNC_PROGRESS);
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals(SYNC_PROGRESS)){

                SyncStatus syncStatus = intent.getParcelableExtra("syncStatus");

                if(syncStatus.isStatus()) {

                    if (syncStatus.getProgress() == 100) {

                        pbDataSyncProgress.setProgress(syncStatus.getProgress());
                        txtProgressText.setText("Fetching server data completed");

                        if (mDatabase.getLoggedInUser() == null) {

                            Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(loginIntent);

                        } else {

                            Intent loginIntent = new Intent(getApplicationContext(), HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            finish();
                            startActivity(loginIntent);
                        }

                    } else {
                        pbDataSyncProgress.setProgress(syncStatus.getProgress());
                        txtProgressText.setText("Fetching server data " + syncStatus.getCurrentTask() + "/" + syncStatus.getTotalTask());

                    }

                }else{
                    laySyncStatus.setVisibility(View.INVISIBLE);
                    laySyncRetryContainer.setVisibility(View.VISIBLE);
                    txtError.setText(syncStatus.getMessage());

                }
            }
        }
    };

}
