package com.spidertechnosoft.xenia.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class OrderDetailListAdapter extends ArrayAdapter<CartItem> {

    private final Context context;
    private final List<CartItem> cartItems;
    private String orderType;
    private CartOperationCallback mCartOperationCallback;
    public OrderDetailListAdapter(@NonNull Context context, @NonNull List<CartItem> objects,@NonNull String orderType,CartOperationCallback mCartOperationCallback) {
        super(context,-1, objects);
        this.context=context;
        this.cartItems=objects;
        this.orderType=orderType;
        this.mCartOperationCallback=mCartOperationCallback;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.lay_order_details_list_item, parent, false);

        TextView txtOrderDetailItemName=(TextView) rowView.findViewById(R.id.txtOrderDetailItemName);

        TextView txtOrderDetailItemQty=(TextView) rowView.findViewById(R.id.txtOrderDetailItemQty);

        TextView txtOrderDetailItemRate=(TextView) rowView.findViewById(R.id.txtOrderDetailItemRate);

        TextView txtOrderDetailItemAmount=(TextView) rowView.findViewById(R.id.txtOrderDetailItemAmount);
        final ImageView btnProductAddToCart=(ImageView) rowView.findViewById(R.id.btnProductAddToCart);

        final ImageView btnProductReduceFromCart=(ImageView) rowView.findViewById(R.id.btnProductReduceFromCart);

        CartItem cartItem=cartItems.get(position);

        Product product=cartItem.getProduct();

        btnProductAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mCartOperationCallback.increaseCartItem(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                txtOrderDetailItemQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }

                Double price=cartItem.getRate();

                if(orderType.equals(OrderType.DINE_IN_NORMAL)){

                    price=product.getsRate()==null?0.00:product.getsRate();
                }else if(orderType.equals(OrderType.DINE_IN_AC)){
                    price=product.getaCRate()==null?0.00:product.getaCRate();
                }else if(orderType.equals(OrderType.TAKE_AWAY)){
                    price=product.getParcelRate()==null?0.00:product.getParcelRate();
                }else if(orderType.equals(OrderType.DOOR_DELIVERY)){
                    price=product.getParcelRate()==null?0.00:product.getParcelRate();
                }


                Double total=cartQty*price;
                txtOrderDetailItemRate.setText(price.toString());
                txtOrderDetailItemAmount.setText(total.toString());

            }
        });

        btnProductReduceFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCartOperationCallback.reduceFromCart(product);

                Integer cartQty=mCartOperationCallback.getCartQty(product);

                txtOrderDetailItemQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }

                Double price=cartItem.getRate();

                if(orderType.equals(OrderType.DINE_IN_NORMAL)){

                    price=product.getsRate()==null?0.00:product.getsRate();
                }else if(orderType.equals(OrderType.DINE_IN_AC)){
                    price=product.getaCRate()==null?0.00:product.getaCRate();
                }else if(orderType.equals(OrderType.TAKE_AWAY)){
                    price=product.getParcelRate()==null?0.00:product.getParcelRate();
                }else if(orderType.equals(OrderType.DOOR_DELIVERY)){
                    price=product.getParcelRate()==null?0.00:product.getParcelRate();
                }


                Double total=cartQty*price;
                txtOrderDetailItemRate.setText(price.toString());
                txtOrderDetailItemAmount.setText(total.toString());
            }
        });

        if(product!=null){


            txtOrderDetailItemName.setText(product.getItemName());

            Integer qty=cartItem.getQty()==null?0:cartItem.getQty().intValue();
            Double price=cartItem.getRate();

            if(orderType.equals(OrderType.DINE_IN_NORMAL)){

                price=product.getsRate()==null?0.00:product.getsRate();
            }else if(orderType.equals(OrderType.DINE_IN_AC)){
                price=product.getaCRate()==null?0.00:product.getaCRate();
            }else if(orderType.equals(OrderType.TAKE_AWAY)){
                price=product.getParcelRate()==null?0.00:product.getParcelRate();
            }else if(orderType.equals(OrderType.DOOR_DELIVERY)){
                price=product.getParcelRate()==null?0.00:product.getParcelRate();
            }


            Double total=qty*price;

            txtOrderDetailItemQty.setText(qty.toString());
            txtOrderDetailItemRate.setText(price.toString());
            txtOrderDetailItemAmount.setText(total.toString());

        }

        return rowView;
    }

}
