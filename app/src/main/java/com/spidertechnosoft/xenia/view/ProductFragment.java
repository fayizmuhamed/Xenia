package com.spidertechnosoft.xenia.view;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Category;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.view.adapter.ProductListAdapter;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductFragment extends Fragment implements CartOperationCallback {


    private Context mContext;

    private View mRootView;

    private LayoutInflater mCurrentInflater;

    private TabLayout mTabCategory;

    private ListView mLvProducts;

    private EditText mEditProductSearch;



    private Database mDatabase;

    ArrayList<Product> mProducts;

    private ProductListAdapter mProductListAdapter;


    // TODO: Rename and change types of parameters
    private Order mOrder;

    private OnFragmentInteractionListener mListener;

    private CartOperationCallback mCartOperationCallback;

    public ProductFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param orderType Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductFragment newInstance(String orderType, String param2) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    public void configView(){


        mProductListAdapter=new ProductListAdapter(mContext,mProducts,mOrder,this);

        mTabCategory=(TabLayout)mRootView.findViewById(R.id.tabCategory);

        mLvProducts=(ListView)mRootView.findViewById(R.id.lvProducts);

        mLvProducts.setAdapter(mProductListAdapter);

        mEditProductSearch=(EditText)mRootView.findViewById(R.id.editProductSearch);

        mEditProductSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(mTabCategory.getSelectedTabPosition()==0){
                    mProductListAdapter.getFilter().filter(s.toString());
                }else{
                    new Handler().postDelayed(
                            new Runnable(){
                                @Override
                                public void run() {
                                    mTabCategory.getTabAt(0).select();
                                }
                            }, 100);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTabCategory.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                Category category=(Category)tab.getTag();
                initializeProduct(category);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void initializeCategory(){

        List<Category> categories=mDatabase.getCategories(Category.Sort.BY_NAME);

        mTabCategory.removeAllTabs();

        if(categories==null||categories.isEmpty()){

            return;
        }

        Category allCategory=new Category();
        allCategory.setCategory("All");
        allCategory.setCategoryId(null);

        TabLayout.Tab tabAll=mTabCategory.newTab();
        tabAll.setText(allCategory.getCategory());
        tabAll.setTag(allCategory);
        mTabCategory.addTab(tabAll);

        for(Category category:categories){

            TabLayout.Tab tab=mTabCategory.newTab();
            tab.setText(category.getCategory());
            tab.setTag(category);
            mTabCategory.addTab(tab);

        }
    }

    public void initializeProduct(Category category){

        mProducts.clear();

        if(category.getCategoryId()==null){

            List<Category> categories=mDatabase.getCategories(Category.Sort.BY_NAME);
            for(Category iterator:categories){

                List<Product> products=mDatabase.getProducts(iterator.getCategoryId().toString(), Product.Sort.BY_NAME);

                if(products==null||products.isEmpty()){
                    continue;
                }

                mProducts.addAll(products);
            }

            mProductListAdapter.getFilter().filter(mEditProductSearch.getText().toString());

        }else {

            List<Product> products = mDatabase.getProducts(category.getCategoryId().toString(), Product.Sort.BY_NAME);

            if (products == null || products.isEmpty()) {

                mProductListAdapter.getFilter().filter(mEditProductSearch.getText().toString());

                return;

            }

            mProducts.addAll(products);

            mProductListAdapter.getFilter().filter(mEditProductSearch.getText().toString());

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Save the context
        this.mContext = getActivity();

        mOrder = ((PlaceOrderActivity)getActivity()).getOrder();

        // Store the inflater reference
        this.mCurrentInflater = inflater;

        this.mRootView = inflater.inflate(R.layout.fragment_product, container, false);


        this.mDatabase= Database.getInstance();

        this.mProducts=new ArrayList<Product>();

        configView();

        initializeCategory();

        return mRootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeCategory();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public boolean increaseCartItem(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            cartItem=new CartItem();
            cartItem.setProduct(product);
            cartItem.setItemId(product.getItemId());
            cartItem.setItemCode(product.getItemCode());
            cartItem.setItemName(product.getItemName());
            cartItem.setCategoryId(product.getCategoryId());

            Double cartQty = 0.00;
            Double rate = mListener.getProductPrice(product);
            cartItem.setQty(cartQty);
            cartItem.setRate(rate);
            Double amount = cartQty*rate;
            cartItem.setAmount(amount);

        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty + 1;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return false;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceAndRemoveFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        if(cartQty<=0){
            mDatabase.removeProductFromCart(product.getItemId());
            mListener.setFooterCartTotal();
            return true;
        }
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean removeFromCart(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        mListener.setFooterCartTotal();

        return true;
    }

    @Override
    public Integer getCartQty(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null||cartItem.getQty()==null) {

            return 0;
        }

        return (cartItem.getQty()==null?0:cartItem.getQty().intValue());

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void setFooterCartTotal();
        Double getProductPrice(Product product);

    }
}
