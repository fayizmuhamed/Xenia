package com.spidertechnosoft.xenia.view;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.RestApiManager;
import com.spidertechnosoft.xenia.model.api.resource.Feedback;
import com.spidertechnosoft.xenia.model.api.resource.Order;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity {

    TextView txtFeedbackName;

    TextView txtFeedbackMobile;

    TextView txtFeedbackEmail;

    TextView txtRating;

    RatingBar rtbFeedbackRating;

    //Get rest api manager
    RestApiManager mRestApiManager;

    Order mOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mRestApiManager=new RestApiManager();
        mOrder=(Order)getIntent().getSerializableExtra(PlaceOrderActivity.ORDER);
        configView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feedback, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_send:
                sendFeedback();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void configView(){
        txtFeedbackName=(TextView)findViewById(R.id.txtFeedbackName);
        txtFeedbackMobile=(TextView)findViewById(R.id.txtFeedbackName);
        txtFeedbackEmail=(TextView)findViewById(R.id.txtFeedbackEmail);
        txtRating=(TextView)findViewById(R.id.txtRating);

        rtbFeedbackRating=(RatingBar)findViewById(R.id.rtbFeedbackRating);

        rtbFeedbackRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                txtRating.setText(rating+"/"+ratingBar.getNumStars());
            }
        });
    }

    public void sendFeedback(){

        Integer orderId=mOrder.getOrderId();

        String name=txtFeedbackName.getText().toString();

        String mobile=txtFeedbackMobile.getText().toString();

        String email=txtFeedbackEmail.getText().toString();

        Double rating=Double.parseDouble(rtbFeedbackRating.getRating()+"");

        if(name==null||name.isEmpty()){

            Toast.makeText(getApplicationContext(),"Please Enter Name",Toast.LENGTH_SHORT).show();

            return ;
        }

        Feedback feedback=new Feedback();
        feedback.setOrderId(orderId);
        feedback.setName(name);
        feedback.setMobile(mobile);
        feedback.setEmail(email);
        feedback.setRating(rating);

        ProgressDialog progressDialog=new ProgressDialog(FeedbackActivity.this);

        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Sending Feedback...");
        progressDialog.show();

        //get user call
        Call<Feedback> sendFeedbackCall=mRestApiManager.getFeedbackClient().sendFeedback(feedback);

        sendFeedbackCall.enqueue(new Callback<Feedback>() {
            @Override
            public void onResponse(Call<Feedback> call, Response<Feedback> response) {

                if(response.isSuccessful()){
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Feedback sending failed",Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<Feedback> call, Throwable t) {
                Toast.makeText(getApplicationContext(),"Feedback sending failed",Toast.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }
}
