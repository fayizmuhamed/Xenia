package com.spidertechnosoft.xenia.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.User;
import com.spidertechnosoft.xenia.model.data.Database;


public class LauncherActivity extends AppCompatActivity {

    ImageButton btnTapIcon;

    Animation shake;

    private Database mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        btnTapIcon=(ImageButton)findViewById(R.id.btnTapIcon);

        shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        mDatabase=Database.getInstance();

        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                if(mDatabase.isFirstTimeUse()){

                    Intent intent = new Intent(LauncherActivity.this,SyncActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(intent);
                }else {

                    if (mDatabase.getLoggedInUser() == null) {


                        Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }else{

                        Intent intent = new Intent(LauncherActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        finish();
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });



        btnTapIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btnTapIcon.startAnimation(shake);

            }
        });


        btnTapIcon.startAnimation(shake);

       /* try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDatabase=Database.getInstance();



        if(mDatabase.isFirstTimeUse()){

            Intent intent = new Intent(this,SyncActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        }else {

            if (mDatabase.getLoggedInUser() == null) {


                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }else{

                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
            }
        }
*/
//        Fabric.with(this, new Crashlytics());

    }
}
