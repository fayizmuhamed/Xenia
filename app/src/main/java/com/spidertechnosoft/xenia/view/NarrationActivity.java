package com.spidertechnosoft.xenia.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.data.Database;

public class NarrationActivity extends AppCompatActivity {

    EditText txtNarration;

    Button btnNarrationCancel;

    Button btnNarrationDone;

    CartItem mCartItem;

    Database mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_narration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDatabase=Database.getInstance();
        mCartItem=(CartItem) getIntent().getSerializableExtra(PlaceOrderActivity.CART_ITEM);
        configView();
        initializeNarrationData();

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void configView(){

        txtNarration=(EditText)findViewById(R.id.txtNarration);

        btnNarrationCancel=(Button)findViewById(R.id.btnNarrationCancel);

        btnNarrationDone=(Button) findViewById(R.id.btnNarrationDone);

        btnNarrationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btnNarrationDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mCartItem.setNarration(txtNarration.getText().toString());

                mDatabase.updateCart(mCartItem);

                finish();
            }
        });
    }

    public void initializeNarrationData(){

        txtNarration.setText(mCartItem.getNarration()==null?"":mCartItem.getNarration());
    }


}
