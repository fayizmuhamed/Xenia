package com.spidertechnosoft.xenia.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;

import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class PendingOrderListAdapter extends ArrayAdapter<Order> {

    private final Context context;
    private final List<Order> orders;

    public PendingOrderListAdapter(@NonNull Context context, @NonNull List<Order> objects) {
        super(context,-1, objects);
        this.context=context;
        this.orders=objects;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.lay_my_orders_list_item, parent, false);



        TextView txtOrderListTableNo=(TextView) rowView.findViewById(R.id.txtOrderListTableNo);

        TextView txtOrderListKOTNo=(TextView) rowView.findViewById(R.id.txtOrderListKOTNo);

        TextView txtOrderListTotalAmount=(TextView) rowView.findViewById(R.id.txtOrderListTotalAmount);

        TextView txtOrderListDate=(TextView) rowView.findViewById(R.id.txtOrderListDate);

        ImageView imgOrderListOrderType=(ImageView)rowView.findViewById(R.id.imgOrderListOrderType);

        Order order=orders.get(position);

        if(order!=null){



            txtOrderListTableNo.setText(order.getTable());
            txtOrderListKOTNo.setText(order.getOrderNo());

            txtOrderListTotalAmount.setText(order.getAmount()+" INR");

            txtOrderListDate.setText(GeneralMethods.convertDateFormat(order.getDate(),GeneralMethods.SERVER_DATE_TIME_FORMAT,GeneralMethods.LOCAL_DATE_TIME_FORMAT));

            if(OrderType.DINE_IN_NORMAL.equals(order.getType())){
                imgOrderListOrderType.setImageResource(R.drawable.ic_dine_in_normal);
            }else if(OrderType.DINE_IN_AC.equals(order.getType())){
                imgOrderListOrderType.setImageResource(R.drawable.ic_dine_in_ac);
            }else if(OrderType.DOOR_DELIVERY.equals(order.getType())){
                imgOrderListOrderType.setImageResource(R.drawable.ic_door_delivery);
            }else if(OrderType.TAKE_AWAY.equals(order.getType())){
                imgOrderListOrderType.setImageResource(R.drawable.ic_take_away);
            }

        }

        return rowView;
    }

}
