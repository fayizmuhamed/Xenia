package com.spidertechnosoft.xenia.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.view.adapter.OrderCartListAdapter;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrderFragment extends Fragment implements CartOperationCallback {

    private Context mContext;
    private Order mOrder;
    private OnFragmentInteractionListener mListener;

    private View mRootView;

    private LayoutInflater mCurrentInflater;

    private SwipeMenuListView lvOrderItems;

    private OrderCartListAdapter mOrderCartListAdapter;

    private LinearLayout layEmptyCart;

    private RelativeLayout btnAddItem;

    List<CartItem> cartItems=new ArrayList<>();

    Database mDatabase;

    public OrderFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Save the context
        this.mContext = getActivity();

        this.mDatabase=Database.getInstance();

        mOrder = ((PlaceOrderActivity)getActivity()).getOrder();

        // Store the inflater reference
        this.mCurrentInflater = inflater;

        this.mRootView = inflater.inflate(R.layout.fragment_order, container, false);

        //initializeProduct();

        configView();

        return mRootView;
    }

    private void configView(){

        layEmptyCart=(LinearLayout)mRootView.findViewById(R.id.layEmptyCart);

        btnAddItem=(RelativeLayout)mRootView.findViewById(R.id.btnAddItem);

        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.goToFragment(null, PlaceOrderActivity.PlaceOrderFragments.PRODUCT_FRAGMENT);
            }
        });

        //Initialisation of product list adapter
        mOrderCartListAdapter=new OrderCartListAdapter(mContext,cartItems,mOrder,this);

        //get list view object
        lvOrderItems=(SwipeMenuListView) mRootView.findViewById(R.id.lvOrderItems);

        lvOrderItems.setAdapter(mOrderCartListAdapter);

        setOrderViewType();

        SwipeMenuCreator swipeMenuCreator=new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                createMenu(menu);
            }

        };

        //set MenuCreator
        lvOrderItems.setMenuCreator(swipeMenuCreator);

        lvOrderItems.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    CartItem cartItem = mOrderCartListAdapter.getItem(position);
                    switch (index) {
                        case 0:
                            reduceAndRemoveFromCart(cartItem.getProduct());
                            break;
                        case 1:
                            Intent intent = new Intent(mContext.getApplicationContext(),NarrationActivity.class);
                            intent.putExtra(PlaceOrderActivity.CART_ITEM,cartItem);
                            mContext.startActivity(intent);
                            break;
                    }
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        initializeOrder();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean increaseCartItem(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            cartItem=new CartItem();
            cartItem.setProduct(product);
            cartItem.setItemId(product.getItemId());
            cartItem.setItemCode(product.getItemCode());
            cartItem.setItemName(product.getItemName());
            cartItem.setCategoryId(product.getCategoryId());

            Double cartQty = 0.00;
            Double rate = mListener.getProductPrice(product);
            cartItem.setQty(cartQty);
            cartItem.setRate(rate);
            Double amount = cartQty*rate;
            cartItem.setAmount(amount);

        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty + 1;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return false;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceAndRemoveFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        if(cartQty<=0){
            mDatabase.removeProductFromCart(product.getItemId());
            mListener.setFooterCartTotal();
            return true;
        }
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        mListener.setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean removeFromCart(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        List<CartItem> cartItemList=mDatabase.getCartContent();

        cartItems.clear();

        if(cartItemList!=null){

            cartItems.addAll(cartItemList);
        }

        mOrderCartListAdapter=new OrderCartListAdapter(mContext,cartItems,mOrder,this);

        lvOrderItems.setAdapter(mOrderCartListAdapter);

        mOrderCartListAdapter.notifyDataSetChanged();

        setOrderViewType();

        mListener.setFooterCartTotal();

        return true;
    }

    @Override
    public Integer getCartQty(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null||cartItem.getQty()==null) {

            return 0;
        }

        return (cartItem.getQty()==null?0:cartItem.getQty().intValue());

    }

    public void initializeOrder(){

        List<CartItem> cartItemList=mDatabase.getCartContent();

        cartItems.clear();

        if(cartItemList!=null){

            cartItems.addAll(cartItemList);
        }

        mOrderCartListAdapter=new OrderCartListAdapter(mContext,cartItems,mOrder,this);

        lvOrderItems.setAdapter(mOrderCartListAdapter);

        mOrderCartListAdapter.notifyDataSetChanged();

        setOrderViewType();

        mListener.setFooterCartTotal();
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        void setFooterCartTotal();
        void goToFragment(Bundle bundle,PlaceOrderActivity.PlaceOrderFragments uri);
        Double getProductPrice(Product product);
    }

    private void createMenu(SwipeMenu menu) {
        SwipeMenuItem item1 = new SwipeMenuItem(getActivity().getApplicationContext());
        item1.setWidth(getResources().getInteger(R.integer.order_delete_button_width));
        item1.setTitle(null);
        item1.setBackground(R.color.delete_button_bg);
        item1.setIcon(R.drawable.icon_delete);
        menu.addMenuItem(item1);
        SwipeMenuItem item2 = new SwipeMenuItem(getActivity().getApplicationContext());
        item2.setWidth(getResources().getInteger(R.integer.order_delete_button_width));
        item2.setTitle(null);
        item2.setBackground(R.color.narration_button_bg);
        item2.setIcon(R.drawable.icon_narration);
        menu.addMenuItem(item2);
    }


    private void setOrderViewType(){

        if(cartItems==null||cartItems.isEmpty()){
            layEmptyCart.setVisibility(View.VISIBLE);
            lvOrderItems.setVisibility(View.GONE);
        }else{
            layEmptyCart.setVisibility(View.GONE);
            lvOrderItems.setVisibility(View.VISIBLE);
        }
    }


    public void showNarrationDialog(CartItem cartItem){

        // Declare the customer dialog
        Dialog dlgCartNarration = new Dialog(getActivity(),R.style.Custom_Dialog);

        // 	SEt no title for the dialog
        dlgCartNarration.requestWindowFeature(Window.FEATURE_NO_TITLE);


        // Set the content view to the customer_alert layout
        dlgCartNarration.setContentView(R.layout.lay_narration_feeding);

        EditText txtNarration=(EditText) dlgCartNarration.findViewById(R.id.txtNarration);

        Button btnNarrationCancel=(Button) dlgCartNarration.findViewById(R.id.btnNarrationCancel);

        Button btnNarrationDone=(Button) dlgCartNarration.findViewById(R.id.btnNarrationDone);

        txtNarration.setText(cartItem.getNarration()==null?"":cartItem.getNarration());

        btnNarrationCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dlgCartNarration.dismiss();
            }
        });

        btnNarrationDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cartItem.setNarration(txtNarration.getText().toString());

                mDatabase.updateCart(cartItem);

                dlgCartNarration.dismiss();
            }
        });


        // Cancel the dialog when touched outside.
        dlgCartNarration.setCanceledOnTouchOutside(false);

        dlgCartNarration.setCancelable(false);

        dlgCartNarration.show();


    }


}
