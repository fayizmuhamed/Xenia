package com.spidertechnosoft.xenia.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.api.resource.Table;

import java.util.List;

/**
 * Created by DELL on 11/22/2017.
 */

public class TableSelectionAdapter extends BaseAdapter {

    Context context;
    private List<Table> tables = null;

    public TableSelectionAdapter(@NonNull Context context, @NonNull List<Table> data) {
        this.context=context;
        this.tables = data;
    }

    @Override
    public int getCount() {
        return tables.size();
    }

    @Override
    public Object getItem(int position) {
        return tables.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.lay_table_selection_item,parent,false);

        TextView txtTableNo=(TextView)view.findViewById(R.id.txtTableNo);

        Table table=tables.get(position);

        txtTableNo.setText(table.getTable());

        return view;

    }


}
