package com.spidertechnosoft.xenia.view;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.model.helper.GeneralMethods;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

public class ProductDetailsActivity extends AppCompatActivity {

    Database mDatabase;

    Order mOrder;

    private TextView txtProductDetailFooterTotal;

    private RelativeLayout btnProductDetailProceed;

    FrameLayout layProductDetailsHead;

    TextView txtProductName;

    TextView txtProductPrice;

    TextView txtProductOrderedQty;

    ImageView btnProductAddToCart;

    ImageView btnProductReduceFromCart;

    TextView txtProductDescription;

    Product mProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        mTitle.setText(toolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDatabase=Database.getInstance();

        mOrder=(Order)getIntent().getSerializableExtra(PlaceOrderActivity.ORDER);

        mProduct=(Product)getIntent().getSerializableExtra(PlaceOrderActivity.PRODUCT);

        configView();

        if(mProduct!=null){
            Picasso.with(this)
                    .load(GeneralMethods.getImageUrl(mProduct.getProductImage()))
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                            BitmapDrawable background=new BitmapDrawable(getResources(), bitmap);

                            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                //noinspection deprecation
                                layProductDetailsHead.setBackgroundDrawable(background);
                            } else {
                                layProductDetailsHead.setBackground(background);
                            }

                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                            layProductDetailsHead.setBackgroundResource(R.color.empty_image_bg);
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            layProductDetailsHead.setBackgroundResource(R.color.empty_image_bg);
                        }
                    });


            txtProductName.setText(mProduct.getItemName());
            txtProductPrice.setText(mProduct.getsRate()+" INR");
            txtProductDescription.setText(mProduct.getDescription());

            String price="";

            switch (mOrder.getType()){

                case OrderType.DINE_IN_NORMAL:
                    price=mProduct.getsRate()==null?"0.00":mProduct.getsRate().toString();
                    break;
                case OrderType.DINE_IN_AC:
                    price=mProduct.getaCRate()==null?"0.00":mProduct.getaCRate().toString();
                    break;
                case OrderType.TAKE_AWAY:
                case OrderType.DOOR_DELIVERY:
                    price=mProduct.getParcelRate()==null?"0.00":mProduct.getParcelRate().toString();
                    break;
            }
            txtProductPrice.setText(price+" INR");

            Integer cartQty=getCartQty(mProduct);

            txtProductOrderedQty.setText(cartQty.toString());

            if(cartQty>0){

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(true);
            }else{

                btnProductAddToCart.setEnabled(true);
                btnProductReduceFromCart.setEnabled(false);
            }
        }

        setFooterCartTotal();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void configView(){

        layProductDetailsHead=(FrameLayout) findViewById(R.id.layProductDetailsHead);

        txtProductName=(TextView) findViewById(R.id.txtProductName);

        txtProductPrice=(TextView) findViewById(R.id.txtProductPrice);

        txtProductDescription=(TextView) findViewById(R.id.txtProductDescription);

        txtProductOrderedQty=(TextView) findViewById(R.id.txtProductOrderedQty);

        btnProductAddToCart=(ImageView) findViewById(R.id.btnProductAddToCart);

        btnProductReduceFromCart=(ImageView) findViewById(R.id.btnProductReduceFromCart);

        txtProductDetailFooterTotal=(TextView) findViewById(R.id.txtProductDetailFooterTotal);

        btnProductDetailProceed=(RelativeLayout) findViewById(R.id.btnProductDetailProceed);

        btnProductAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                increaseCartItem(mProduct);

                Integer cartQty=getCartQty(mProduct);

                txtProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }
            }
        });

        btnProductReduceFromCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                reduceAndRemoveFromCart(mProduct);

                Integer cartQty=getCartQty(mProduct);

                txtProductOrderedQty.setText(cartQty.toString());

                if(cartQty>0){

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(true);
                }else{

                    btnProductAddToCart.setEnabled(true);
                    btnProductReduceFromCart.setEnabled(false);
                }
            }
        });

        btnProductDetailProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public boolean increaseCartItem(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            cartItem=new CartItem();
            cartItem.setProduct(product);
            cartItem.setItemId(product.getItemId());
            cartItem.setItemCode(product.getItemCode());
            cartItem.setItemName(product.getItemName());
            cartItem.setCategoryId(product.getCategoryId());

            Double cartQty = 0.00;
            Double rate = getProductPrice(product);
            cartItem.setQty(cartQty);
            cartItem.setRate(rate);
            Double amount = cartQty*rate;
            cartItem.setAmount(amount);

        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty + 1;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    public boolean reduceFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return false;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    public boolean reduceAndRemoveFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        if(cartQty<=0){
            mDatabase.removeProductFromCart(product.getItemId());
            setFooterCartTotal();
            return true;
        }
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    public boolean removeFromCart(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        setFooterCartTotal();

        return true;
    }

    public Integer getCartQty(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return 0;
        }

        return  (cartItem.getQty()==null?0:cartItem.getQty().intValue());

    }




    public void setFooterCartTotal(){

        List<CartItem> cartItems=mDatabase.getCartContent();

        if(cartItems==null){

            txtProductDetailFooterTotal.setText("0.00");

            return;
        }

        Double total=0.00;

        for(CartItem cartItem:cartItems){

            Double itemPrice=cartItem.getRate();

            Double itemQty=cartItem.getQty();

            total=total+(itemPrice* itemQty);

        }

        txtProductDetailFooterTotal.setText(total.toString());
    }

    public Double getProductPrice(Product product){

        Double price=0.00;

        switch (mOrder.getType()){

            case OrderType.DINE_IN_NORMAL:
                price=product.getsRate()==null?0.00:product.getsRate();
                break;
            case OrderType.DINE_IN_AC:
                price=product.getaCRate()==null?0.00:product.getaCRate();
                break;
            case OrderType.TAKE_AWAY:
            case OrderType.DOOR_DELIVERY:
                price=product.getParcelRate()==null?0.00:product.getParcelRate();
                break;
            default:
                price=0.00;
        }

        return price;
    }

}
