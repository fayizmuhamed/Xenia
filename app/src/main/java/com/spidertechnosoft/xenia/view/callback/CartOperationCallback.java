package com.spidertechnosoft.xenia.view.callback;

import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Product;

import java.util.HashMap;

/**
 * Created by DELL on 12/30/2017.
 */

public interface CartOperationCallback {


    public boolean increaseCartItem(Product product);

    public boolean reduceFromCart(Product product);

    public boolean reduceAndRemoveFromCart(Product product);

    public boolean removeFromCart(Product product);

    public Integer getCartQty(Product product);
}
