package com.spidertechnosoft.xenia.view;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.RestApiManager;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.OrderDetails;
import com.spidertechnosoft.xenia.model.api.resource.OrderType;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.view.adapter.OrderFragmentPagerAdapter;
import com.spidertechnosoft.xenia.view.controls.CustomViewPager;
import com.spidertechnosoft.xenia.view.callback.CartOperationCallback;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceOrderActivity extends AppCompatActivity implements ProductFragment.OnFragmentInteractionListener,
        OrderFragment.OnFragmentInteractionListener,
        TableFragment.OnFragmentInteractionListener,
        OrderDetailFragment.OnFragmentInteractionListener,
        CartOperationCallback {

    public static enum PlaceOrderFragments {
        PRODUCT_FRAGMENT,
        ORDER_FRAGMENT,
        TABLE_FRAGMENT,
        ORDER_DETAIL_FRAGMENT
    }

    private Order order;

    public static final String ORDER = "ORDER";

    public static final String PRODUCT = "PRODUCT";

    public static final String CART_ITEM = "CART_ITEM";

    public static final String CART = "CART";

    private TextView mTextPlaceOrderFooterTotal;


    private RelativeLayout mButtonPlaceOrderFooterProceed;

    private RelativeLayout mButtonPlaceOrderFooterSave;

    private RelativeLayout mFlContent;

    private LinearLayout layPlaceOrderFooter;

    private int mCurrentPage=0;

    private TextView mTitle;
    private Toolbar mToolbar;

    private Database mDatabase;

    //initialize fragment manager
    FragmentManager fragmentManager;

    //initialize fragment transaction object
    FragmentTransaction fragmentTransaction;

    private Stack<PlaceOrderFragments> fragmentStack=new Stack<>();

    Dialog dlgPrintingProgress;

    //Get rest api manager
    RestApiManager mRestApiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);
        mDatabase= Database.getInstance();
        mRestApiManager=new RestApiManager();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTitle = (TextView) mToolbar.findViewById(R.id.toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setActivityTitle(0,R.string.title_activity_product_selection);

        Order orderIn=(Order)getIntent().getSerializableExtra(ORDER);

        if(orderIn==null){

            Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        }

        order=orderIn;

        configView();

        setFooterCartTotal();

        if(order.getOrderId()==null||order.getOrderId()==0){

            if(order.getType()==null||order.getType().isEmpty()){

                Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);

            }else{

                goToFragment(null,PlaceOrderFragments.PRODUCT_FRAGMENT);
            }

        }else{

            List<OrderDetails> orderDetailsList=order.getItems();

            if(orderDetailsList!=null&& !orderDetailsList.isEmpty()){

                for(OrderDetails orderDetails: orderDetailsList){
                    CartItem cartItem=new CartItem();
                    cartItem.setItemId(orderDetails.getItemId());
                    cartItem.setItemCode(orderDetails.getItemCode());
                    cartItem.setItemName(orderDetails.getItemName());
                    cartItem.setCategoryId(orderDetails.getCategoryId());
                    cartItem.setQty(orderDetails.getQty());
                    cartItem.setRate(orderDetails.getRate());
                    cartItem.setAmount(orderDetails.getAmount());
                    cartItem.setNarration(orderDetails.getNarration());
                    Product product=mDatabase.getProduct(orderDetails.getCategoryId(),orderDetails.getItemId());

                    if(product!=null){

                        cartItem.setProduct(product);
                    }

                    mDatabase.updateCart(cartItem);
                }
            }

            goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
        }

    }

    private void setActivityTitle(Integer type,Integer resourceId){

        if(type==null||type==0){

            mToolbar.setBackgroundResource(R.color.colorPrimary);

        }else{
            mToolbar.setBackgroundResource(R.drawable.order_action_bar_bg);
        }

        mTitle.setText(getResources().getString(resourceId));
    }

    private void configView(){

        mFlContent=(RelativeLayout)findViewById(R.id.flContent);

        layPlaceOrderFooter=(LinearLayout)findViewById(R.id.layPlaceOrderFooter);


        mTextPlaceOrderFooterTotal=(TextView) findViewById(R.id.txtPlaceOrderFooterTotal);

        mButtonPlaceOrderFooterProceed=(RelativeLayout) findViewById(R.id.btnPlaceOrderFooterProceed);

        mButtonPlaceOrderFooterSave=(RelativeLayout) findViewById(R.id.btnPlaceOrderFooterSave);


        mButtonPlaceOrderFooterProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(fragmentStack.lastElement().equals(PlaceOrderFragments.PRODUCT_FRAGMENT)){

                    //goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);

                    if(validateCart()){

                        if (order.getType() .equals(OrderType.DINE_IN_NORMAL) || order.getType().equals(OrderType.DINE_IN_AC)) {

                            if(order.getTableId()==null){

                                //push dashboard to stack
                                goToFragment(null,PlaceOrderFragments.TABLE_FRAGMENT);

                            }else{

                                //push dashboard to stack
                                goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
                            }



                        }else{

                            //push dashboard to stack
                            goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
                        }
                    }

                }else if(fragmentStack.lastElement().equals(PlaceOrderFragments.ORDER_FRAGMENT)){

                    if(validateCart()){

                        if (order.getType() .equals(OrderType.DINE_IN_NORMAL) || order.getType().equals(OrderType.DINE_IN_AC)) {

                            if(order.getTableId()==null){

                                //push dashboard to stack
                                goToFragment(null,PlaceOrderFragments.TABLE_FRAGMENT);

                            }else{

                                //push dashboard to stack
                                goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
                            }



                        }else{

                            //push dashboard to stack
                            goToFragment(null,PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);
                        }
                    }

                }

            }
        });

        mButtonPlaceOrderFooterSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postOrder();
            }
        });
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the state of item position
        outState.putSerializable(ORDER, order);

    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // Read the state of item position
        order =(Order) savedInstanceState.getSerializable(ORDER);

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        setFooterCartTotal();
    }

    @Override
    public void onBackPressed() {

        //pop last one from stack
        fragmentStack.pop();

        if (fragmentStack.size() > 0){

            goToFragment(null, fragmentStack.lastElement());

        }else{
            super.onBackPressed();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:

                //pop last one from stack
                fragmentStack.pop();

                if (fragmentStack.size() > 0){

                    goToFragment(null, fragmentStack.lastElement());

                }else{
                    super.onBackPressed();
                }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {


    }

    public Order getOrder() {
        return order;
    }


    @Override
    public boolean increaseCartItem(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            cartItem=new CartItem();
            cartItem.setProduct(product);
            cartItem.setItemId(product.getItemId());
            cartItem.setItemCode(product.getItemCode());
            cartItem.setItemName(product.getItemName());
            cartItem.setCategoryId(product.getCategoryId());

            Double cartQty = 0.00;
            Double rate = getProductPrice(product);
            cartItem.setQty(cartQty);
            cartItem.setRate(rate);
            Double amount = cartQty*rate;
            cartItem.setAmount(amount);

        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty + 1;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return false;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);

        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean reduceAndRemoveFromCart(Product product) {


        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        Double cartQty = cartItem.getQty();
        Double rate = cartItem.getRate();
        cartQty = cartQty - 1;
        if(cartQty<=0){
            mDatabase.removeProductFromCart(product.getItemId());
            setFooterCartTotal();
            return true;
        }
        cartQty=cartQty>0?cartQty:0;
        cartItem.setQty(cartQty);
        Double amount = cartQty*rate;
        cartItem.setAmount(amount);
        mDatabase.updateCart(cartItem);

        setFooterCartTotal();

        // Return true;
        return true;
    }

    @Override
    public boolean removeFromCart(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null) {

            return true;
        }

        mDatabase.removeProductFromCart(product.getItemId());

        setFooterCartTotal();

        return true;
    }

    @Override
    public Integer getCartQty(Product product) {

        CartItem cartItem = mDatabase.getCartContentForProduct(product.getItemId());

        // If the cartContent is null, then just add the list
        if (cartItem == null||cartItem.getQty()==null) {

            return 0;
        }

        return (cartItem.getQty()==null?0:cartItem.getQty().intValue());

    }


    public void setFooterCartTotal(){

        List<CartItem> cartItems=mDatabase.getCartContent();

        if(cartItems==null){

            mTextPlaceOrderFooterTotal.setText("0.00");

            return;
        }

        Double total=0.00;

        for(CartItem cartItem:cartItems){

            Double itemPrice=cartItem.getRate();

            Double itemQty=cartItem.getQty();

            total=total+(itemPrice* itemQty);

        }

        mTextPlaceOrderFooterTotal.setText(total.toString());
    }

    @Override
    protected void onDestroy() {


        super.onDestroy();
    }

    /**
     * Function called to move the fragment
     *
     * @param bundle    : The bundle object
     * @param uri       : The URI with the path
     */
    public void goToFragment(Bundle bundle,PlaceOrderFragments uri){

        //Variable to hold fragment
        Fragment fragment;

        //set fragment manager
        fragmentManager=getSupportFragmentManager();


        //get fragment transaction
        fragmentTransaction=fragmentManager.beginTransaction();

        //set animation for change in fragment
        //  fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_left);


        if(uri.equals(PlaceOrderFragments.PRODUCT_FRAGMENT)){

            //instantiate dashboard fragment
            fragment = new ProductFragment();

            if(bundle!=null) {

                fragment.setArguments(bundle);
            }
            if (fragment != null) {

                setActivityTitle(0,R.string.title_activity_product_selection);

                //clearSeatMapFragment();

                //replace content
                fragmentTransaction.replace(R.id.flContent, fragment, PlaceOrderFragments.PRODUCT_FRAGMENT.toString());

                //clear fragment stack
                fragmentStack.clear();

                //push dashboard to stack
                fragmentStack.push(PlaceOrderFragments.PRODUCT_FRAGMENT);

                layPlaceOrderFooter.setVisibility(View.VISIBLE);

                mButtonPlaceOrderFooterProceed.setVisibility(View.VISIBLE);

                mButtonPlaceOrderFooterSave.setVisibility(View.GONE);

                //commit fragment transaction
                fragmentTransaction.commit();


            }
        }else if(uri.equals(PlaceOrderFragments.ORDER_FRAGMENT)){

            //instantiate InFlightSaleSelectFlightFragment
            fragment = new OrderFragment();
            if(bundle!=null) {

                fragment.setArguments(bundle);
            }
            if (fragment != null) {

                setActivityTitle(1,R.string.title_activity_order);

                //clearSeatMapFragment();

                //replace content
                fragmentTransaction.replace(R.id.flContent, fragment, PlaceOrderFragments.PRODUCT_FRAGMENT.toString());

                //clear fragment stack
                fragmentStack.clear();

                fragmentStack.push(PlaceOrderFragments.PRODUCT_FRAGMENT);

                //push dashboard to stack
                //fragmentStack.push(PlaceOrderFragments.ORDER_FRAGMENT);

                layPlaceOrderFooter.setVisibility(View.VISIBLE);

                mButtonPlaceOrderFooterProceed.setVisibility(View.VISIBLE);

                mButtonPlaceOrderFooterSave.setVisibility(View.GONE);

                //commit fragment transaction
                fragmentTransaction.commit();

            }
        }else if(uri.equals(PlaceOrderFragments.TABLE_FRAGMENT)) {

            //instantiate uplift fragment
            fragment = new TableFragment();

            if(bundle!=null) {

                fragment.setArguments(bundle);
            }
            if (fragment != null) {

                setActivityTitle(1,R.string.title_activity_table_selection);

                //clearSeatMapFragment();

                //replace content
                fragmentTransaction.replace(R.id.flContent, fragment, PlaceOrderFragments.PRODUCT_FRAGMENT.toString());

                //clear fragment stack
                fragmentStack.clear();

                fragmentStack.push(PlaceOrderFragments.PRODUCT_FRAGMENT);

                //push dashboard to stack
                //fragmentStack.push(PlaceOrderFragments.ORDER_FRAGMENT);

                //push dashboard to stack
                fragmentStack.push(PlaceOrderFragments.TABLE_FRAGMENT);

                layPlaceOrderFooter.setVisibility(View.GONE);

                mButtonPlaceOrderFooterProceed.setVisibility(View.GONE);

                mButtonPlaceOrderFooterSave.setVisibility(View.GONE);

                //commit fragment transaction
                fragmentTransaction.commit();
            }



        }else if(uri.equals(PlaceOrderFragments.ORDER_DETAIL_FRAGMENT)) {

            //instantiate inventory check fragment
            fragment = new OrderDetailFragment();
            if (bundle != null) {

                fragment.setArguments(bundle);
            }
            if (fragment != null) {

                setActivityTitle(1, R.string.title_activity_order_details);

                //clearSeatMapFragment();

                //replace content
                fragmentTransaction.replace(R.id.flContent, fragment, PlaceOrderFragments.PRODUCT_FRAGMENT.toString());

                //clear fragment stack
                fragmentStack.clear();

                fragmentStack.push(PlaceOrderFragments.PRODUCT_FRAGMENT);

                //push dashboard to stack
               // fragmentStack.push(PlaceOrderFragments.ORDER_FRAGMENT);

                if (order.getType().equals(OrderType.DINE_IN_NORMAL) || order.getType().equals(OrderType.DINE_IN_AC)) {

                    //push dashboard to stack
                    fragmentStack.push(PlaceOrderFragments.TABLE_FRAGMENT);
                }

                //push dashboard to stack
                fragmentStack.push(PlaceOrderFragments.ORDER_DETAIL_FRAGMENT);

                layPlaceOrderFooter.setVisibility(View.VISIBLE);

                mButtonPlaceOrderFooterProceed.setVisibility(View.GONE);

                mButtonPlaceOrderFooterSave.setVisibility(View.VISIBLE);

                //commit fragment transaction
                fragmentTransaction.commit();
            }

        }
    }


    public void postOrder(){

        if(validateCart()) {

            // Declare the customer dialog
            dlgPrintingProgress = new Dialog(PlaceOrderActivity.this);

            // 	SEt no title for the dialog
            dlgPrintingProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);

            // Set the content view to the customer_alert layout
            dlgPrintingProgress.setContentView(R.layout.layout_custom_printing_progress);

            ProgressBar progress = (ProgressBar) dlgPrintingProgress.findViewById(R.id.pbPrintingProgress);
            progress.setIndeterminate(true);

            // Cancel the dialog when touched outside.
            dlgPrintingProgress.setCanceledOnTouchOutside(false);

            dlgPrintingProgress.setCancelable(false);

            dlgPrintingProgress.show();

            List<CartItem> cartItems=mDatabase.getCartContent();

            List<OrderDetails> items=new ArrayList<>();
            Double total=0.0;

            for(CartItem cartItem:cartItems){

                OrderDetails orderDetails=new OrderDetails();
                orderDetails.setItemId(cartItem.getItemId());
                orderDetails.setItemCode(cartItem.getItemCode());
                orderDetails.setItemName(cartItem.getItemName());
                orderDetails.setCategoryId(cartItem.getCategoryId());
                orderDetails.setQty(cartItem.getQty());
                orderDetails.setRate(cartItem.getRate());
                orderDetails.setAmount(cartItem.getAmount());
                orderDetails.setNarration(cartItem.getNarration());

                items.add(orderDetails);
                Double qty=cartItem.getQty();
                Double rate=cartItem.getRate();
                total=total+(qty* rate);
            }

            order.setAmount(total);
            order.setItems(items);

            //get user call
            Call<Order> postOrderCall = mRestApiManager.getOrderClient().saveOrder(order);

            postOrderCall.enqueue(new Callback<Order>() {
                @Override
                public void onResponse(Call<Order> call, Response<Order> response) {

                    if (response.isSuccessful()) {
                        mDatabase.clearCart();
                        order=null;
                        fragmentStack.clear();
                        dlgPrintingProgress.dismiss();
                        finish();
                    } else {
                        dlgPrintingProgress.dismiss();
                        Toast.makeText(getApplicationContext(), "Order save failed", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<Order> call, Throwable t) {
                    dlgPrintingProgress.dismiss();
                    Toast.makeText(getApplicationContext(), "Order save failed", Toast.LENGTH_SHORT).show();
                    t.printStackTrace();
                }
            });

        }

    }

    public boolean validateCart(){

        List<CartItem> cartItems=mDatabase.getCartContent();

        if(cartItems==null||cartItems.isEmpty()){

            Toast.makeText(getApplicationContext(),"No Items in cart",Toast.LENGTH_SHORT).show();
            return false;
        }

        for(CartItem cartItem:cartItems){

            if(cartItem.getQty()==null||cartItem.getQty()==0){

                Toast.makeText(getApplicationContext(),"Product "+cartItem.getProduct().getItemName()+" with zero qty",Toast.LENGTH_SHORT).show();
                return false;
            }
        }

        return true;
    }

    public Double getProductPrice(Product product){

        Double price=0.00;

        switch (order.getType()){

            case OrderType.DINE_IN_NORMAL:
                price=product.getsRate()==null?0.00:product.getsRate();
                break;
            case OrderType.DINE_IN_AC:
                price=product.getaCRate()==null?0.00:product.getaCRate();
                break;
            case OrderType.TAKE_AWAY:
            case OrderType.DOOR_DELIVERY:
                price=product.getParcelRate()==null?0.00:product.getParcelRate();
                break;
            default:
                price=0.00;
        }

        return price;
    }
}
