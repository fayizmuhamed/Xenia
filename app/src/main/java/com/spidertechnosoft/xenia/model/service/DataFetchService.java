package com.spidertechnosoft.xenia.model.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.api.RestApiManager;
import com.spidertechnosoft.xenia.model.api.client.UserClient;
import com.spidertechnosoft.xenia.model.api.resource.Category;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.api.resource.Table;
import com.spidertechnosoft.xenia.model.api.resource.User;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.view.SyncActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by DELL on 12/27/2017.
 */

public class DataFetchService extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private static final String TAG = DataFetchService.class.getName();

    private Database mDatabase;

    private int totalTask=4;

    SyncStatus syncStatus=new SyncStatus();

    public DataFetchService() {
        super("DataFetchService");
        mDatabase=Database.getInstance();
        syncStatus.setTotalTask(totalTask);
        syncStatus.setCurrentTask(0);
        syncStatus.setStatus(true);

    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;



    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews remoteViews = new RemoteViews(getPackageName(),
                R.layout.custom_notification);

        notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon_data_sync)
                .setContentTitle("Sync")
                .setContentText("Fetching data")
                .setAutoCancel(true);


        notificationManager.notify(0, notificationBuilder.build());




        initDataSync();
    }

    public void initDataSync(){

        Log.d(TAG, "Data fetching started!");

        //Get rest api manager
        RestApiManager restApiManager=new RestApiManager();

        sendNotification(syncStatus);

        //get user call
        Call<List<User>> userCall=restApiManager.getUserClient().getUsers();

        //get category call
        Call<List<Category>> categoryCall=restApiManager.getCategoryClient().getCategories();

        //get products call
        Call<List<Product>> productCall=restApiManager.getProductClient().getProducts();

        //get table call
        Call<List<Table>> tableCall=restApiManager.getTableClient().getTables();

        userCall.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> userCall, Response<List<User>> userResponse) {

                mDatabase.deleteUsers();

                if(userResponse.isSuccessful()&&userResponse.body()!=null&&!userResponse.body().isEmpty()){

                    for(User user:userResponse.body()){

                        mDatabase.saveUser(user);
                    }

                    syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                    syncStatus.setStatus(true);
                    sendNotification(syncStatus);

                    tableCall.enqueue(new Callback<List<Table>>() {
                        @Override
                        public void onResponse(Call<List<Table>> tableCall, Response<List<Table>> tableResponse) {

                            mDatabase.deleteTable();

                            if(tableResponse.isSuccessful()&&tableResponse.body()!=null&&!tableResponse.body().isEmpty()){

                                for(Table table:tableResponse.body()){

                                    mDatabase.saveTable(table);
                                }

                                syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                syncStatus.setStatus(true);
                                sendNotification(syncStatus);

                                categoryCall.enqueue(new Callback<List<Category>>() {
                                    @Override
                                    public void onResponse(Call<List<Category>> categoryCall, Response<List<Category>> categoryResponse) {

                                        mDatabase.deleteCategories();

                                        if(categoryResponse.isSuccessful()&&categoryResponse.body()!=null&&!categoryResponse.body().isEmpty()){

                                            for(Category category:categoryResponse.body()){

                                                mDatabase.saveCategory(category);


                                            }

                                            syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                            syncStatus.setStatus(true);
                                            sendNotification(syncStatus);

                                            productCall.enqueue(new Callback<List<Product>>() {
                                                @Override
                                                public void onResponse(Call<List<Product>> productCall, Response<List<Product>> productResponse) {

                                                    mDatabase.deleteProduct();

                                                    if(productResponse.isSuccessful()&&productResponse.body()!=null&&!productResponse.body().isEmpty()){

                                                        for(Product product:productResponse.body()){

                                                            mDatabase.saveProduct(product);


                                                        }

                                                        syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                                        syncStatus.setStatus(true);
                                                        sendNotification(syncStatus);



                                                    }else {

                                                        Log.d("Error", "No data response");
                                                        syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                                        syncStatus.setStatus(false);
                                                        syncStatus.setMessage(userResponse.code()+": No product data response");
                                                        sendNotification(syncStatus);
                                                    }

                                                }

                                                @Override
                                                public void onFailure(Call<List<Product>> call, Throwable t) {

                                                    syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                                    syncStatus.setStatus(false);
                                                    syncStatus.setMessage(t.getMessage());
                                                    sendNotification(syncStatus);
                                                }
                                            });


                                        }else {

                                            Log.d("Error", "No data response");
                                            syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                            syncStatus.setStatus(false);
                                            syncStatus.setMessage(userResponse.code()+": No category data response");
                                            sendNotification(syncStatus);
                                        }

                                    }

                                    @Override
                                    public void onFailure(Call<List<Category>> call, Throwable t) {

                                        syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                        syncStatus.setStatus(false);
                                        syncStatus.setMessage(t.getMessage());
                                        sendNotification(syncStatus);
                                    }
                                });

                            }else {

                                Log.d("Error", "No data response");
                                syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                                syncStatus.setMessage(userResponse.code()+": No table data response");
                                syncStatus.setStatus(false);
                                sendNotification(syncStatus);
                            }

                        }

                        @Override
                        public void onFailure(Call<List<Table>> call, Throwable t) {

                            syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                            syncStatus.setMessage(t.getMessage());
                            syncStatus.setStatus(false);
                            sendNotification(syncStatus);
                        }
                    });



                }else {

                    Log.d("Error", "No data response");
                    syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                    syncStatus.setMessage(userResponse.code()+": No user data response");
                    syncStatus.setStatus(false);
                    sendNotification(syncStatus);
                }

            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

                syncStatus.setCurrentTask(syncStatus.getCurrentTask()+1);
                syncStatus.setMessage(t.getMessage());
                syncStatus.setStatus(false);
                sendNotification(syncStatus);
            }
        });







    }

    private void sendNotification(SyncStatus syncStatus){

        syncStatus.setProgress((syncStatus.getCurrentTask()/syncStatus.getTotalTask())*100);

        sendIntent(syncStatus);

        if(syncStatus.isStatus()){

            notificationBuilder.setProgress(100,syncStatus.getProgress(),false);
            notificationBuilder.setContentText("Fetching server data "+syncStatus.getCurrentTask()+"/"+syncStatus.getTotalTask());
            notificationManager.notify(0, notificationBuilder.build());

            if(syncStatus.getProgress()==100){

                mDatabase.setFirstTimeUsage(false);
                notificationManager.cancel(0);
            }
        }else{
            notificationManager.cancel(0);
        }

    }

    private void sendIntent(SyncStatus syncStatus){

        Intent intent = new Intent(SyncActivity.SYNC_PROGRESS);
        intent.putExtra("syncStatus",syncStatus);
        LocalBroadcastManager.getInstance(DataFetchService.this).sendBroadcast(intent);
    }




    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }
}
