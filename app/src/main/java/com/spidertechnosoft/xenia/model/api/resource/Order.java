package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DELL on 11/24/2017.
 */

public class Order implements Serializable {


    @SerializedName("OrderId")
    @Expose
    private Integer orderId;

    @SerializedName("OrderNo")
    @Expose
    private String orderNo;

    @SerializedName("OrderType")
    @Expose
    private String orderType;

    @SerializedName("OrderDate")
    @Expose
    private String orderDate;

    @SerializedName("TableId")
    @Expose
    private Integer tableId;

    @SerializedName("Table")
    @Expose
    private String table;

    @SerializedName("PAX")
    @Expose
    private String pax;

    @SerializedName("Amount")
    @Expose
    private Double amount;

    @SerializedName("Type")
    @Expose
    private String type;

    @SerializedName("Date")
    @Expose
    private String date;

    @SerializedName("Ready")
    @Expose
    private Boolean ready;

    @SerializedName("Processed")
    @Expose
    private Boolean processed;

    @SerializedName("Cancelled")
    @Expose
    private Boolean cancelled;

    @SerializedName("WaiterId")
    @Expose
    private Integer waiterId;

    @SerializedName("WaiterName")
    @Expose
    private String waiterName;

    @SerializedName("Items")
    @Expose
    List<OrderDetails> items=new ArrayList<OrderDetails>();

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getPax() {
        return pax;
    }

    public void setPax(String pax) {
        this.pax = pax;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getReady() {
        return ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Integer getWaiterId() {
        return waiterId;
    }

    public void setWaiterId(Integer waiterId) {
        this.waiterId = waiterId;
    }

    public String getWaiterName() {
        return waiterName;
    }

    public void setWaiterName(String waiterName) {
        this.waiterName = waiterName;
    }

    public List<OrderDetails> getItems() {
        return items;
    }

    public void setItems(List<OrderDetails> items) {
        this.items = items;
    }
}
