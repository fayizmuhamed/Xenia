package com.spidertechnosoft.xenia.model.api;

import com.spidertechnosoft.xenia.model.api.client.CategoryClient;
import com.spidertechnosoft.xenia.model.api.client.FeedbackClient;
import com.spidertechnosoft.xenia.model.api.client.OrderClient;
import com.spidertechnosoft.xenia.model.api.client.ProductClient;
import com.spidertechnosoft.xenia.model.api.client.TableClient;
import com.spidertechnosoft.xenia.model.api.client.UserClient;
import com.spidertechnosoft.xenia.model.api.resource.Table;
import com.spidertechnosoft.xenia.model.data.Database;
import com.spidertechnosoft.xenia.model.helper.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by DELL on 12/21/2017.
 */

public class RestApiManager {

    private ProductClient mProductClient;

    private CategoryClient mCategoryClient;

    private TableClient mTableClient;

    private UserClient mUserClient;

    private OrderClient mOrderClient;

    private FeedbackClient mFeedbackClient;

    private Database mDatabase=Database.getInstance();

    public ProductClient getProductClient(){

        if(mProductClient==null){

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mProductClient=retrofit.create(ProductClient.class);
        }

        return mProductClient;
    }


    public CategoryClient getCategoryClient(){

        if(mCategoryClient==null){

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mCategoryClient=retrofit.create(CategoryClient.class);
        }

        return mCategoryClient;
    }


    public TableClient getTableClient(){

        if(mTableClient==null){

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mTableClient=retrofit.create(TableClient.class);
        }

        return mTableClient;
    }

    public UserClient getUserClient(){
        String url=mDatabase.getBaseUrl();
        if(mUserClient==null){


            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mUserClient=retrofit.create(UserClient.class);
        }

        return mUserClient;
    }

    public OrderClient getOrderClient(){

        if(mOrderClient==null){

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mOrderClient=retrofit.create(OrderClient.class);
        }

        return mOrderClient;
    }

    public FeedbackClient getFeedbackClient(){

        if(mFeedbackClient==null){

            Retrofit retrofit=new Retrofit.Builder()
                    .baseUrl(mDatabase.getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mFeedbackClient=retrofit.create(FeedbackClient.class);
        }

        return mFeedbackClient;
    }
}
