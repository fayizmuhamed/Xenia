package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class Category implements Serializable {

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("Category")
    @Expose
    private String category;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", category='" + category + '\'' +
                '}';
    }


    public enum Sort {
        BY_ID,
        BY_NAME;

        private Comparator<Category> comparator;

        private Sort () {
            switch (ordinal()) {
                case 0: {
                    comparator = (lhs, rhs) -> lhs.getCategoryId() - rhs.getCategoryId();
                    break;
                }
                case 1: {
                    comparator = (lhs, rhs) -> lhs.getCategory().compareTo(rhs.getCategory());
                    break;
                }
            }
        }

        public Comparator<Category> getComparator() {
            return comparator;
        }
    }
}
