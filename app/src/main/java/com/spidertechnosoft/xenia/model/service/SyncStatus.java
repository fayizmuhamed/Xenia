package com.spidertechnosoft.xenia.model.service;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by DELL on 12/27/2017.
 */

public class SyncStatus implements Parcelable{

    public SyncStatus() {
    }
    private boolean status;
    private String message;
    private int progress;
    private int currentTask;
    private int totalTask;


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(int currentTask) {
        this.currentTask = currentTask;
    }

    public int getTotalTask() {
        return totalTask;
    }

    public void setTotalTask(int totalTask) {
        this.totalTask = totalTask;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(progress);
        dest.writeInt(currentTask);
        dest.writeInt(totalTask);
    }

    private SyncStatus(Parcel in) {

        progress = in.readInt();
        currentTask = in.readInt();
        totalTask = in.readInt();
    }

    public static final Parcelable.Creator<SyncStatus> CREATOR = new Parcelable.Creator<SyncStatus>() {
        public SyncStatus createFromParcel(Parcel in) {
            return new SyncStatus(in);
        }

        public SyncStatus[] newArray(int size) {
            return new SyncStatus[size];
        }
    };
}
