package com.spidertechnosoft.xenia.model.api.resource;

/**
 * Created by DELL on 12/28/2017.
 */

public  class OrderType {
    public static final String  DINE_IN_NORMAL="Dine In Normal";
    public static final String  DINE_IN_AC="Dine In AC";
    public static final String  TAKE_AWAY="Take Away";
    public static final String  DOOR_DELIVERY="Door Delivery";
}
