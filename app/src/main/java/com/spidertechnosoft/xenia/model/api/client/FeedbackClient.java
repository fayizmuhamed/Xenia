package com.spidertechnosoft.xenia.model.api.client;

import com.spidertechnosoft.xenia.model.api.resource.Feedback;
import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.helper.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by DELL on 12/21/2017.
 */

public interface FeedbackClient {

    @POST(Constants.HTTP.POST_FEEDBACK_API)
    Call<Feedback> sendFeedback(@Body Feedback feedback);
}
