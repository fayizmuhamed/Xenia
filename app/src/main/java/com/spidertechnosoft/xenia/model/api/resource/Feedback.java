package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class Feedback implements Serializable {

    @SerializedName("FeedbackId")
    @Expose
    private Integer feedbackId;

    @SerializedName("OrderId")
    @Expose
    private Integer orderId;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Mobile")
    @Expose
    private String mobile;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("Rating")
    @Expose
    private Double rating;

    public Integer getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(Integer feedbackId) {
        this.feedbackId = feedbackId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }
}
