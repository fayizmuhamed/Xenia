package com.spidertechnosoft.xenia.model.helper;

/**
 * Created by DELL on 12/21/2017.
 */

public class Constants {

    public static final class HTTP{

        public static final String BASE_URL="http://xeniapos.stslive.in/";
        public static final String GET_PRODUCT_API="api/item";
        public static final String GET_CATEGORY_API="api/category";
        public static final String GET_TABLE_API="api/table";
        public static final String GET_USER_API="api/user";
        public static final String GET_PENDING_ORDER_API="api/order/getPendingOrders";
        public static final String PRINT_ORDER_API="api/order/printOrder/{id}";
        public static final String CANCEL_ORDER_API="api/order/cancelOrder/{id}";
        public static final String SAVE_ORDER_API="api/order/saveOrder";
        public static final String POST_FEEDBACK_API="api/feedback";
    }
}
