package com.spidertechnosoft.xenia.model.api.client;

import com.spidertechnosoft.xenia.model.api.resource.Order;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.helper.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by DELL on 12/21/2017.
 */

public interface OrderClient {

    @GET(Constants.HTTP.GET_PENDING_ORDER_API)
    Call<List<Order>> getPendingOrders();

    @POST(Constants.HTTP.SAVE_ORDER_API)
    Call<Order> saveOrder(@Body Order order);

    @POST(Constants.HTTP.CANCEL_ORDER_API)
    Call<String> cancelOrder(@Path("id") Integer orderId);

    @POST(Constants.HTTP.PRINT_ORDER_API)
    Call<String> printOrder(@Path("id") Integer orderId);
}
