package com.spidertechnosoft.xenia.model.helper;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.spidertechnosoft.xenia.R;
import com.spidertechnosoft.xenia.model.data.Database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DELL on 11/24/2017.
 */

public class GeneralMethods {

    public static final String SERVER_DATE_TIME_FORMAT="yyyy-MM-dd'T'HH:mm:ss";

    public static final String LOCAL_DATE_TIME_FORMAT="yyyy-MM-dd hh:mm aa";

    public static final String LOCAL_DATE_FORMAT="yyyy-MM-dd";

    public static String getDateInSpecifiedFormat(Date date,String format){

        if(date==null){

            return "";
        }

        // Create the formatter
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        String dateInSpecifiedFormat=dateFormat.format(date);

        return dateInSpecifiedFormat;
    }

    public static Date getDateFromSpecifiedFormatString(String strDate,String format) {


        try {

            // Create the formatter
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);

            // Parse the date
            Date date = dateFormat.parse(strDate);

            // Return the date long value
            return date;

        } catch (ParseException e) {

            // Return -1
            return null;

        }
    }

    public static String convertDateFormat(String strDate,String fromFormat,String toFormat) {


        try {

            // Create the formatter
            SimpleDateFormat sdfFrom = new SimpleDateFormat(fromFormat);

            // Parse the date
            Date date = sdfFrom.parse(strDate);

            // Create the formatter
            SimpleDateFormat sdfTo = new SimpleDateFormat(toFormat);

            String dateInSpecifiedFormat=sdfTo.format(date);

            // Return the date long value
            return dateInSpecifiedFormat;

        } catch (ParseException e) {

            // Return -1
            return null;

        }
    }

    public static String getImageUrl(String imageUrl){

        Database mDatabase=Database.getInstance();

        String baseUrl=mDatabase.getBaseUrl();

        if(baseUrl==null||baseUrl.isEmpty()){

            return imageUrl;
        }

        baseUrl+=imageUrl;

        return baseUrl;
    }

}
