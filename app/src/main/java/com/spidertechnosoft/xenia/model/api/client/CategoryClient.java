package com.spidertechnosoft.xenia.model.api.client;

import com.spidertechnosoft.xenia.model.api.resource.Category;
import com.spidertechnosoft.xenia.model.helper.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by DELL on 12/21/2017.
 */

public interface CategoryClient {

    @GET(Constants.HTTP.GET_CATEGORY_API)
    Call<List<Category>> getCategories();
}
