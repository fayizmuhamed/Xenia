package com.spidertechnosoft.xenia.model.api.client;

import com.spidertechnosoft.xenia.model.helper.Constants;
import com.spidertechnosoft.xenia.model.api.resource.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by DELL on 12/21/2017.
 */

public interface ProductClient {

    @GET(Constants.HTTP.GET_PRODUCT_API)
    Call<List<Product>> getProducts();
}
