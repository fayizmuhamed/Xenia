package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class Product implements Serializable {

    @SerializedName("ItemId")
    @Expose
    private Integer itemId;

    @SerializedName("ItemCode")
    @Expose
    private String itemCode;

    @SerializedName("ItemName")
    @Expose
    private String itemName;

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;

    @SerializedName("Category")
    @Expose
    private String category;

    @SerializedName("SRate")
    @Expose
    private Double sRate;

    @SerializedName("ParcelRate")
    @Expose
    private Double parcelRate;

    @SerializedName("ACRate")
    @Expose
    private Double aCRate;

    @SerializedName("Description")
    @Expose
    private String description;

    @SerializedName("Unit")
    @Expose
    private String unit;

    @SerializedName("Image")
    @Expose
    private String productImage;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getsRate() {
        return sRate;
    }

    public void setsRate(Double sRate) {
        this.sRate = sRate;
    }

    public Double getParcelRate() {
        return parcelRate;
    }

    public void setParcelRate(Double parcelRate) {
        this.parcelRate = parcelRate;
    }

    public Double getaCRate() {
        return aCRate;
    }

    public void setaCRate(Double aCRate) {
        this.aCRate = aCRate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }



    public enum Sort {
        BY_ID,
        BY_NAME;

        private Comparator<Product> comparator;

        private Sort () {
            switch (ordinal()) {
                case 0: {
                    comparator = (lhs, rhs) -> lhs.getItemId() - rhs.getItemId();
                    break;
                }
                case 1: {
                    comparator = (lhs, rhs) -> lhs.getItemName().compareTo(rhs.getItemName());
                    break;
                }
            }
        }

        public Comparator<Product> getComparator() {
            return comparator;
        }
    }
}
