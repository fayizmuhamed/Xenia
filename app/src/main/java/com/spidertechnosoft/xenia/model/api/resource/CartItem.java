package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class CartItem implements Serializable {

    @SerializedName("ItemId")
    @Expose
    private Integer itemId;

    @SerializedName("ItemCode")
    @Expose
    private String itemCode;

    @SerializedName("ItemName")
    @Expose
    private String itemName;

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;

    @SerializedName("Qty")
    @Expose
    private Double qty;

    @SerializedName("Rate")
    @Expose
    private Double rate;

    @SerializedName("Amount")
    @Expose
    private Double Amount;

    @SerializedName("Narration")
    @Expose
    private String narration;

    @SerializedName("Product")
    @Expose
    private Product product;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public String getNarration() {
        return narration;
    }

    public void setNarration(String narration) {
        this.narration = narration;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
