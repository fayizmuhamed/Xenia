package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class User implements Serializable {
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public enum Sort {
        BY_ID,
        BY_NAME;

        private Comparator<User> comparator;

        private Sort () {
            switch (ordinal()) {
                case 0: {
                    comparator = (lhs, rhs) -> lhs.getUserId() - rhs.getUserId();
                    break;
                }
                case 1: {
                    comparator = (lhs, rhs) -> lhs.getUserName().compareTo(rhs.getUserName());
                    break;
                }
            }
        }

        public Comparator<User> getComparator() {
            return comparator;
        }
    }
}
