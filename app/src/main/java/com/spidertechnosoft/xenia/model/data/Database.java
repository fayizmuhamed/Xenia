package com.spidertechnosoft.xenia.model.data;

import android.app.Application;
import android.content.Context;

import com.snappydb.DB;
import com.snappydb.SnappyDB;
import com.snappydb.SnappydbException;
import com.spidertechnosoft.xenia.model.api.resource.CartItem;
import com.spidertechnosoft.xenia.model.api.resource.Category;
import com.spidertechnosoft.xenia.model.api.resource.Product;
import com.spidertechnosoft.xenia.model.api.resource.Table;
import com.spidertechnosoft.xenia.model.api.resource.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by DELL on 12/27/2017.
 */

public class Database {

    private static volatile Database sDatabase;

    private static DB snappydb;

    private Context mContext;

    //private constructor.
    public Database(Application application){
        try {
            snappydb = new SnappyDB.Builder(application)
                    .name("xenia")
                    .build();
            if(!snappydb.exists("first_time_use")){

                snappydb.putBoolean("first_time_use",true);
            }

            sDatabase=this;

            mContext=application;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Empty the internal DB
     */
    public void empty() {
        try {
            snappydb.destroy();
            snappydb = new SnappyDB.Builder(mContext)
                    .name("xenia")
                    .build();
            if(!snappydb.exists("first_time_use")){

                snappydb.putBoolean("first_time_use",true);
            }

            sDatabase=this;

        } catch (SnappydbException e) {
            e.printStackTrace();
        }
    }



    public static Database getInstance() {
        return sDatabase;
    }

    //Make singleton from serialize and deserialize operation.
    protected Database readResolve() {
        return getInstance();
    }

    public Product saveProduct(Product product) {
        synchronized (snappydb) {
            String key = "tag:" + product.getCategoryId() + ":product:" + product.getItemId();
            try {
                snappydb.put(key, product);

            } catch (SnappydbException e) {
                e.printStackTrace();
                // Observable.error(e);
            }
            return product;
        }
    }

    public List<Product> getProducts(String category, Product.Sort... sort) {
        synchronized (snappydb) {
            ArrayList<Product> products = new ArrayList<>();

            try {
                for (String key : snappydb.findKeys("tag:" + category + ":product:")) {
                    products.add(snappydb.get(key, Product.class));
                }

                // sort
                if (null != sort && sort.length > 0) {// Use provided sort
                    Collections.sort(products, sort[0].getComparator());

                } else {//Use default sort
                    Collections.sort(products, Product.Sort.BY_NAME.getComparator());
                }

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return products;

        }
    }

    public Product getProduct(Integer categoryId,Integer productId) {

        synchronized (snappydb) {

            try {
                String key = "tag:" + categoryId + ":product:" + productId;
                if (snappydb.exists(key)) {

                    return snappydb.get(key, Product.class);
                }

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public void deleteProduct() {

        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("tag:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Category> getCategories(Category.Sort... sort) {
        synchronized (snappydb) {
            ArrayList<Category> categories = new ArrayList<>();

            try {
                for (String key : snappydb.findKeys("category:")) {
                    categories.add(snappydb.get(key, Category.class));
                }

               /* // sort
                if (null != sort && sort.length > 0) {// Use provided sort
                    Collections.sort(categories, sort[0].getComparator());

                } else {//Use default sort
                    Collections.sort(categories, Category.Sort.BY_ID.getComparator());
                }*/

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return categories;

        }
    }

    public Category saveCategory(Category category) {
        synchronized (snappydb) {
            String key = "category:" + category.getCategoryId();
            try {
                // Save to database
                snappydb.put(key, category);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return category;//for chaining operators
        }
    }

    public void deleteCategories() {

        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("category:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }

    public List<User> getUsers(User.Sort... sort) {
        synchronized (snappydb) {
            ArrayList<User> users = new ArrayList<>();

            try {
                for (String key : snappydb.findKeys("user:")) {
                    users.add(snappydb.get(key, User.class));
                }

                // sort
                if (null != sort && sort.length > 0) {// Use provided sort
                    Collections.sort(users, sort[0].getComparator());

                } else {//Use default sort
                    Collections.sort(users, User.Sort.BY_ID.getComparator());
                }

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return users;

        }
    }


    public User getUser(String username) {
        synchronized (snappydb) {

            try {
                String key="user:"+username;
                if(snappydb.exists(key)){

                    return snappydb.get(key, User.class);
                }

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return null;

        }
    }


    public User saveUser(User user) {
        synchronized (snappydb) {
            String key = "user:" + user.getUserName();
            try {
                // Save to database
                snappydb.put(key, user);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return user;//for chaining operators
        }
    }

    public void deleteUsers() {

        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("user:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }


    public Table saveTable(Table table) {
        synchronized (snappydb) {
            String key = "tableType:" + table.getType()+":table:"+table.getTableId();
            try {
                // Save to database
                snappydb.put(key, table);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return table;//for chaining operators
        }
    }

    public List<Table> getTables(String type,Table.Sort... sort) {

        synchronized (snappydb) {
                ArrayList<Table> tables = new ArrayList<>();

                try {
                    for (String key : snappydb.findKeys("tableType:" + type+":table:")) {
                        tables.add(snappydb.get(key, Table.class));
                    }

                    // sort
                    if (null != sort && sort.length > 0) {// Use provided sort
                        Collections.sort(tables, sort[0].getComparator());

                    } else {//Use default sort
                        Collections.sort(tables, Table.Sort.BY_ID.getComparator());
                    }

                } catch (SnappydbException e) {
                    e.printStackTrace();
                }

                return tables;


        }
    }

    public void deleteTable() {

        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("tableType:")) {
                    snappydb.del(key);
                }
            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }

    public String getBaseIp() {
        synchronized (snappydb) {
            try {
                if(snappydb.exists("base")){

                    return snappydb.get("base");
                }

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
        return "";
    }

    public String getBaseUrl() {
        synchronized (snappydb) {
            try {
                if(snappydb.exists("base")){

                    return "http://"+snappydb.get("base")+":9030/";
                }

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
        return "";
    }

    public void setBaseUrl(String url) {
        synchronized (snappydb) {
            try {
                snappydb.put("base",url);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }

    public boolean isFirstTimeUse() {
        synchronized (snappydb) {
            try {
                return snappydb.exists("first_time_use")?snappydb.getBoolean("first_time_use"):true;

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
        return true;
    }


    public void setFirstTimeUsage(Boolean status) {
        synchronized (snappydb) {
            try {
                snappydb.putBoolean("first_time_use",status);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
        }
    }


    public User getLoggedInUser() {
        synchronized (snappydb) {
            User user=null;
            String key = "session:";
            try {
                // Save to database
                user=snappydb.get(key, User.class);

                return user;

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return user;//for chaining operators
        }
    }

    public User setLoggedInUser(User user) {
        synchronized (snappydb) {
            String key = "session:";
            try {
                // Save to database
                snappydb.put(key, user);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return user;//for chaining operators
        }
    }
    public Boolean removeLoggedInUser() {
        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("session:")) {
                    snappydb.del(key);
                }
                return true;

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }

            return false;
        }
    }

    /**
     * Method to update the cart in the shared pref
     *
     * @param cartItem - The cart Item list
     */
    public CartItem updateCart(CartItem cartItem) {

        synchronized (snappydb) {
            String key = "cart:"+cartItem.getItemId();
            try {
                // Save to database
                snappydb.put(key, cartItem);

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }
            return cartItem;//for chaining operators
        }


    }


    /**
     * Function to return the cartItem list
     * Returns null if the data is not available
     */
    public List<CartItem> getCartContent() {

        synchronized (snappydb) {
            ArrayList<CartItem> cartItems = new ArrayList<>();

            try {

                for (String key : snappydb.findKeys("cart:")) {
                    cartItems.add(snappydb.get(key, CartItem.class));
                }


            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return cartItems;

        }
    }

    /**
     * Function to return the cartItem list
     * Returns null if the data is not available
     */
    public CartItem getCartContentForProduct(Integer productId) {

        synchronized (snappydb) {

            try {

                String key="cart:"+productId;
                if(snappydb.exists(key)){

                    return snappydb.get(key, CartItem.class);
                }


            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return null;

        }
    }

    /**
     * Function to return the cartItem list
     * Returns null if the data is not available
     */
    public Boolean removeProductFromCart(Integer productId) {

        synchronized (snappydb) {

            try {

                for (String key : snappydb.findKeys("cart:"+productId)) {
                    snappydb.del(key);
                }
                return true;

            } catch (SnappydbException e) {
                e.printStackTrace();
            }

            return false;

        }
    }


    /**
     * Puts the cart to empty string
     */
    public Boolean clearCart() {

        synchronized (snappydb) {
            try {
                for (String key : snappydb.findKeys("cart:")) {
                    snappydb.del(key);
                }
                return true;

            } catch (SnappydbException e) {
                e.printStackTrace();//TODO handle Exception elegantly (throw Observable.error)
            }

            return false;
        }

    }
}
