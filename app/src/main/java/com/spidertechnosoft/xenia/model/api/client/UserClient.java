package com.spidertechnosoft.xenia.model.api.client;


import com.spidertechnosoft.xenia.model.api.resource.User;
import com.spidertechnosoft.xenia.model.helper.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by DELL on 12/21/2017.
 */

public interface UserClient {

    @GET(Constants.HTTP.GET_USER_API)
    Call<List<User>> getUsers();

    @GET(Constants.HTTP.GET_USER_API)
    Observable<List<User>> getUserList();

    class BackendResponse<T> {
        List<T> data;

        public List<T> getData() {
            return data;
        }

        public void setData(List<T> data) {
            this.data = data;
        }
    }
}
