package com.spidertechnosoft.xenia.model.api.resource;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by DELL on 11/24/2017.
 */

public class Table implements Serializable {

    @SerializedName("TableId")
    @Expose
    private Integer tableId;

    @SerializedName("Table")
    @Expose
    private String table;

    @SerializedName("Type")
    @Expose
    private String type;

    public Integer getTableId() {
        return tableId;
    }

    public void setTableId(Integer tableId) {
        this.tableId = tableId;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tableId=" + tableId +
                ", table='" + table + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public enum Sort {
        BY_ID,
        BY_NAME;

        private Comparator<Table> comparator;

        private Sort () {
            switch (ordinal()) {
                case 0: {
                    comparator = (lhs, rhs) -> lhs.getTableId() - rhs.getTableId();
                    break;
                }
                case 1: {
                    comparator = (lhs, rhs) -> lhs.getTable().compareTo(rhs.getTable());
                    break;
                }
            }
        }

        public Comparator<Table> getComparator() {
            return comparator;
        }
    }
}
