package com.spidertechnosoft.xenia;

import android.app.Application;
import android.os.Environment;

import com.spidertechnosoft.xenia.model.data.Database;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.io.File;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;

/**
 * Created by DELL on 12/25/2017.
 */

public class XeniaApp extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        new Database(this);
        
        Picasso.Builder builder = new Picasso.Builder(this);
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }
}
